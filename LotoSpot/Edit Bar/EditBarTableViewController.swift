//
//  EditBarTableViewController.swift
//  LotoSpot
//
//  Created by Bret Rideout on 3/21/17.
//  Copyright © 2017 RapidWare. All rights reserved.
//

import UIKit
import Firebase

class EditBarTableViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var barName: UITextField!
    @IBOutlet weak var barAddress: UITextField!
    @IBOutlet weak var barLat: UITextField!
    @IBOutlet weak var barLon: UITextField!
    @IBOutlet weak var barAboutUs: UITextView!
    @IBOutlet weak var mondayHours: UITextField!
    @IBOutlet weak var tuesdayHours: UITextField!
    @IBOutlet weak var wenesdayHours: UITextField!
    @IBOutlet weak var thursdayHours: UITextField!
    @IBOutlet weak var fridayHours: UITextField!
    @IBOutlet weak var saturdayHours: UITextField!
    @IBOutlet weak var sundayHours: UITextField!
    @IBOutlet weak var uncoveredParking: UITextField!
    @IBOutlet weak var coveredParking: UITextField!
    @IBOutlet weak var slipLengths: UITextField!
    @IBOutlet weak var waterBarSwitch: UISwitch!
    @IBOutlet weak var barMenuSite: UITextField!
    @IBOutlet weak var barPhoneNumber: UITextField!
    @IBOutlet weak var barWebsite: UITextField!
    @IBOutlet weak var visibleSwith: UISwitch!
    @IBOutlet weak var barImg: UIImageView!
    
    var barKey: String = String()
    var bars: [Bar] = []
    var bar: Bar!
    var barType: String = String()
    var imagePicker: UIImagePickerController!
    var imageSelected = false
    var hiddenBar = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagePicker = UIImagePickerController()
        self.imagePicker.allowsEditing = true
        self.imagePicker.delegate = self
        self.barName.text = bar.barName
        self.barAddress.text = bar.barAddress
        self.barLat.text = bar.barLat
        self.barLon.text = bar.barLon
        self.barAboutUs.text = bar.barDescription
        self.mondayHours.text = bar.barMonday
        self.tuesdayHours.text = bar.barTuesday
        self.wenesdayHours.text = bar.barWednesday
        self.thursdayHours.text = bar.barThursday
        self.fridayHours.text = bar.barFriday
        self.saturdayHours.text = bar.barSaturday
        self.sundayHours.text = bar.barSunday
        self.barMenuSite.text = bar.barMenuSite
        self.barPhoneNumber.text = bar.barPhoneNumber
        self.barWebsite.text = bar.barWebsite
        if self.hiddenBar {
            self.visibleSwith.isOn = false
        }
        if self.barType == "water" {
            self.waterBarSwitch.isOn = true
            if let uncoveredParkingBar = bar.barParkingCovered {
                self.uncoveredParking.text = String(describing: uncoveredParkingBar)
            }
            if let coveredParkingBar = bar.barParkingUncovered {
                self.coveredParking.text = String(describing: coveredParkingBar)
            }
            self.slipLengths.text = bar.barParkingLength
        } else {
            self.waterBarSwitch.isOn = false
        }
        self.navigationItem.title = "Edit Bar"
        if let barImageURL = bar.barImage {
            if let imgCache = LandBarDetailTableViewController.imageCache.object(forKey: barImageURL as NSString) {
                self.barImg.image = imgCache
            }
        }
    }

    @IBAction func saveTabbed(_ sender: Any) {
        guard let name = barName.text, name != "", let address = barAddress.text, address != "", let lat = barLat.text, lat != "", let lon = barLon.text, lon != "", let aboutUs = barAboutUs.text, aboutUs != "", let monday = mondayHours.text, monday != "", let tuesday = tuesdayHours.text, tuesday != "", let wenesday = wenesdayHours.text, wenesday != "", let thursday = thursdayHours.text, thursday != "", let friday = fridayHours.text, friday != "", let saturday = saturdayHours.text, saturday != "", let sunday = sundayHours.text, sunday != "", let menuSite = barMenuSite.text, menuSite != "", let phoneNumber = barPhoneNumber.text, phoneNumber != "", let website = barWebsite.text, website != ""  else {
            showErrorAlert("Error", msg: "No field can be blank.", View: self)
            return
        }
        if imageSelected == false {
            self.postToFirebase(imgURL: bar.barImage as! String)
        } else {
            guard let img = barImg.image, imageSelected == true else {
                print("BRET: No image selected")
                showErrorAlert("Error", msg: "An image must be selected", View: self)
                return
            }
            
            if let imgData = UIImageJPEGRepresentation(img, 0.2) {
                let imgUID = NSUUID().uuidString
                let metaData = FIRStorageMetadata()
                metaData.contentType = "image/jpeg"
                DataService.ds.REF_BAR_IMAGES.child(imgUID).put(imgData, metadata: metaData) { (metadata, error) in
                    if error != nil {
                        showErrorAlert("Error", msg: "Error: \(error.debugDescription)", View: self)
                        return
                    } else {
                        let imageURL = metadata?.downloadURL()?.absoluteString
                        if let url = imageURL {
                            self.postToFirebase(imgURL: url)
                        } else {
                            showErrorAlert("ERROR", msg: "Bad url", View: self)
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func selectImage(_ sender: Any) {
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.barImg.image = image
            self.imageSelected = true
        } else {
            print("BRET: A valid image was not selected")
        }
        self.imagePicker.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Helper Functions
    
    func postToFirebase(imgURL: String) {
        var bar: [String: Any] = [:]
        var bar2: [String: Any] = [:]
        var visible: Bool = true
        if visibleSwith.isOn {
            visible = true
        } else {
            visible = false
        }
        bar = (["hours":["monday": mondayHours.text!, "tuesday": tuesdayHours.text!, "wednesday": wenesdayHours.text!, "thursday": thursdayHours.text!, "friday":fridayHours.text!, "saturday":saturdayHours.text!, "sunday":sundayHours.text!], "coordinates":["lat":barLat.text!, "lon":barLon.text!], "parking":["covered":coveredParking.text!, "uncovered":uncoveredParking.text!, "length":slipLengths.text!], "name":barName.text!, "visible":visible, "desc":barAboutUs.text!, "address":barAddress.text!, "menuSite":barMenuSite.text!, "phoneNumber":barPhoneNumber.text!, "website":barWebsite.text!, "imageURL": imgURL])
        
        bar2 = ["hours":["monday": mondayHours.text!, "tuesday": tuesdayHours.text!, "wednesday": wenesdayHours.text!, "thursday": thursdayHours.text!, "friday":fridayHours.text!, "saturday":saturdayHours.text!, "sunday":sundayHours.text!], "coordinates":["lat":barLat.text!, "lon":barLon.text!], "name":barName.text!, "visible":visible, "desc":barAboutUs.text!, "address":barAddress.text!, "menuSite":barMenuSite.text!, "phoneNumber":barPhoneNumber.text!, "website":barWebsite.text!, "imageURL": imgURL]
        
        if waterBarSwitch.isOn == false {
            if barType == "water" {
                let waterBarReference = DataService.ds.REF_WATER_BARS.child(barKey)
                waterBarReference.updateChildValues(bar2)
                DataService.ds.REF_WATER_BARS.child(barKey).observe(.value, with: { snapshot in
                    let landBarReference = DataService.ds.REF_LAND_BARS.childByAutoId()
                    landBarReference.setValue(snapshot.value)
                    waterBarReference.removeValue()
                })
            } else {
                let landBarReference = DataService.ds.REF_LAND_BARS.child(barKey)
                landBarReference.updateChildValues(bar2)
            }
            if hiddenBar {
                self.successAlert(segueType: SEGUE_UNWIND_TO_HIDDEN_BAR)
            } else {
                self.successAlert(segueType: SEGUE_UNWIND_TO_LAND_BAR)
            }
            
        } else {
            guard let convered = uncoveredParking.text, convered != "", let uncovered = uncoveredParking.text, uncovered != "", let lengths = slipLengths.text, lengths != "" else {
                showErrorAlert("Error", msg: "No field can be blank", View: self)
                return
            }
            if barType == "water" {
                let waterBarReference = DataService.ds.REF_WATER_BARS.child(barKey)
                waterBarReference.updateChildValues(bar)
            } else {
                let landBarReference = DataService.ds.REF_LAND_BARS.child(barKey)
                landBarReference.updateChildValues(bar)
                DataService.ds.REF_LAND_BARS.child(barKey).observe(.value, with: { snapshot in
                    let waterBarReference = DataService.ds.REF_WATER_BARS.childByAutoId()
                    waterBarReference.setValue(snapshot.value)
                    landBarReference.removeValue()
                })
            }
            if hiddenBar {
                self.successAlert(segueType: SEGUE_UNWIND_TO_HIDDEN_BAR)
            } else {
                self.successAlert(segueType: SEGUE_UNWIND_TO_WATER_BAR)
            }
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 22
    }
    
    func successAlert(segueType: String) {
        let alert = UIAlertController(title: "Success", message: "Bar has been updated.", preferredStyle: .alert)
        //                    let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        let action = UIAlertAction(title: "Ok", style: .default, handler: { (nil) in
            self.performSegue(withIdentifier: segueType, sender: self)
        })
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }

}

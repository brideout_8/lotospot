//
//  TaxiTypeTableViewController.swift
//  LotoSpot
//
//  Created by Bret Rideout on 3/26/17.
//  Copyright © 2017 RapidWare. All rights reserved.
//

import UIKit

class TaxiTypeTableViewController: UITableViewController {
    
    var taxiType = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundColor = UIColor.init(red: CGFloat(241/255.0), green: CGFloat(241/255.0), blue: CGFloat(241/255.0), alpha: CGFloat(1.0))
        self.navigationItem.title = "Taxi"
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 2
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            self.taxiType = "cab"
            performSegue(withIdentifier: SEGUE_TAXI_DETAIL, sender: self)
        }
        
        if indexPath.row == 1 {
            self.taxiType = "water"
            performSegue(withIdentifier: SEGUE_TAXI_DETAIL, sender: self)
        }
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SEGUE_TAXI_DETAIL {
            if let destination = segue.destination as? TaxiDetailTableViewController {
                destination.taxiType = self.taxiType
            }
        }
    }
}

//
//  BarMenuCell.swift
//  LotoSpot
//
//  Created by Debra Rideout on 5/26/16.
//  Copyright © 2016 RapidWare. All rights reserved.
//

import UIKit

class BarMenuCell: UITableViewCell {

    @IBOutlet weak var barMenu: UILabel!
    
//    var bar: Bar!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(_ menuItem: String) {
//        self.bar = bar
        
        self.barMenu.text = menuItem
    }
}

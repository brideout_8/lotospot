//
//  BarReviewCell.swift
//  LotoSpot
//
//  Created by Debra Rideout on 5/28/16.
//  Copyright © 2016 RapidWare. All rights reserved.
//

import UIKit
import Firebase

class BarReviewCell: UITableViewCell {

    @IBOutlet weak var barReview: UITextView!
    @IBOutlet weak var starOne: UIImageView!
    @IBOutlet weak var starTwo: UIImageView!
    @IBOutlet weak var starThree: UIImageView!
    @IBOutlet weak var starFour: UIImageView!
    @IBOutlet weak var starFive: UIImageView!
    @IBOutlet weak var name: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(_ review: String, stars: Int, name: String) {
        
        self.barReview.text = review
        
        DataService.ds.REF_USERS.child(name).observeSingleEvent(of: .value, with: { snapshot
            in
            let displayName = (snapshot.value as? NSDictionary)!["name"] as! String
            self.name.text = displayName
            
        })
        
        
        
        if stars == 5 {
            starOne.image = UIImage(named: "Star Full")
            starTwo.image = UIImage(named: "Star Full")
            starThree.image = UIImage(named: "Star Full")
            starFour.image = UIImage(named: "Star Full")
            starFive.image = UIImage(named: "Star Full")
        } else if stars == 4 {
            starOne.image = UIImage(named: "Star Full")
            starTwo.image = UIImage(named: "Star Full")
            starThree.image = UIImage(named: "Star Full")
            starFour.image = UIImage(named: "Star Full")
            starFive.image = UIImage(named: "Star Empty")
        } else if stars == 3 {
            starOne.image = UIImage(named: "Star Full")
            starTwo.image = UIImage(named: "Star Full")
            starThree.image = UIImage(named: "Star Full")
            starFour.image = UIImage(named: "Star Empty")
            starFive.image = UIImage(named: "Star Empty")
        } else if stars == 2 {
            starOne.image = UIImage(named: "Star Full")
            starTwo.image = UIImage(named: "Star Full")
            starThree.image = UIImage(named: "Star Empty")
            starFour.image = UIImage(named: "Star Empty")
            starFive.image = UIImage(named: "Star Empty")
        } else if stars == 6 {
            starOne.image = UIImage(named: "Star Full")
            starTwo.image = UIImage(named: "Star Empty")
            starThree.image = UIImage(named: "Star Empty")
            starFour.image = UIImage(named: "Star Empty")
            starFive.image = UIImage(named: "Star Empty")
        }

    }

}

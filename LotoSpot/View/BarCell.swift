//
//  BarCell.swift
//  LotoSpot
//
//  Created by Bret Rideout on 5/24/16.
//  Copyright © 2016 RapidWare. All rights reserved.
//

import UIKit
import Firebase
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class BarCell: UITableViewCell {
    
    @IBOutlet weak var barName: UILabel!
    @IBOutlet weak var checkInCount: UILabel!
    @IBOutlet weak var beerMug: UIImageView!
    @IBOutlet weak var starOne: UIImageView!
    @IBOutlet weak var starTwo: UIImageView!
    @IBOutlet weak var starThree: UIImageView!
    @IBOutlet weak var starFour: UIImageView!
    @IBOutlet weak var starFive: UIImageView!
    @IBOutlet weak var barImg: UIImageView!
    
    var bar: Bar!
    var barRating = 0
    var barStars = [Int]()

    override func awakeFromNib() {
        super.awakeFromNib()
//        self.barImg.roundedImage()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(_ bar: Bar, img: UIImage? = nil) {
        self.bar = bar
        
        self.barName.text = bar.barName
        if bar.barEventName?.count > 0 {
            beerMug.image = UIImage(named: "beermug")
        } else {
            //Empty Photo
            beerMug.image = nil
        }
        if bar.barCheckIns != nil {
            self.checkInCount.text = "\(bar.barCheckIns!.count)"
        } else {
            self.checkInCount.text = "0"
        }
        if img != nil {
            self.barImg.image = img
        } else {
            if let barImgURL = bar.barImage as? String {
                let ref = FIRStorage.storage().reference(forURL: barImgURL)
                ref.data(withMaxSize: 2 * 1024 * 1024, completion: { (data, error) in
                    if error != nil {
                        print("error downloading photo")
                        print(error.debugDescription)
                    } else {
                        print("Downloaded")
                        if let imageData = data {
                            if let img = UIImage(data: imageData) {
                                self.barImg.image = img
                                LandBarDetailTableViewController.imageCache.setObject(img, forKey:bar.barImage! as NSString)
                            }
                        }
                    }
                })
            }
        }
        if bar.barStars?.count > 0 {
            for star in bar.barStars! {
                if star == 6 {
                    barRating = barRating + 1
                } else {
                    barRating = barRating + star
                }
            }
            barRating = barRating / bar.barStars!.count
        } else {
            barRating = 0
        }
        
        
        if barRating == 0 {
            starOne.image = UIImage(named: "Star Empty")
            starTwo.image = UIImage(named: "Star Empty")
            starThree.image = UIImage(named: "Star Empty")
            starFour.image = UIImage(named: "Star Empty")
            starFive.image = UIImage(named: "Star Empty")
        } else if barRating == 1 {
            starOne.image = UIImage(named: "Star Full")
            starTwo.image = UIImage(named: "Star Empty")
            starThree.image = UIImage(named: "Star Empty")
            starFour.image = UIImage(named: "Star Empty")
            starFive.image = UIImage(named: "Star Empty")
        } else if barRating <= 2 {
            starOne.image = UIImage(named: "Star Full")
            starTwo.image = UIImage(named: "Star Full")
            starThree.image = UIImage(named: "Star Empty")
            starFour.image = UIImage(named: "Star Empty")
            starFive.image = UIImage(named: "Star Empty")
        } else if barRating <= 3 {
            starOne.image = UIImage(named: "Star Full")
            starTwo.image = UIImage(named: "Star Full")
            starThree.image = UIImage(named: "Star Full")
            starFour.image = UIImage(named: "Star Empty")
            starFive.image = UIImage(named: "Star Empty")
        } else if barRating <= 4 {
            starOne.image = UIImage(named: "Star Full")
            starTwo.image = UIImage(named: "Star Full")
            starThree.image = UIImage(named: "Star Full")
            starFour.image = UIImage(named: "Star Full")
            starFive.image = UIImage(named: "Star Empty")
        } else if barRating > 4 {
            starOne.image = UIImage(named: "Star Full")
            starTwo.image = UIImage(named: "Star Full")
            starThree.image = UIImage(named: "Star Full")
            starFour.image = UIImage(named: "Star Full")
            starFive.image = UIImage(named: "Star Full")
        } else {
            starOne.image = UIImage(named: "Star Empty")
            starTwo.image = UIImage(named: "Star Empty")
            starThree.image = UIImage(named: "Star Empty")
            starFour.image = UIImage(named: "Star Empty")
            starFive.image = UIImage(named: "Star Empty")
        }
    }
}

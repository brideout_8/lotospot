//
//  TaxiCellTableViewCell.swift
//  LotoSpot
//
//  Created by Bret Rideout on 3/26/17.
//  Copyright © 2017 RapidWare. All rights reserved.
//

import UIKit

class TaxiCellTableViewCell: UITableViewCell {
    
    @IBOutlet weak var taxiName: UILabel!
    @IBOutlet weak var taxiNumber: UIButton!
    
    var taxiNumberCall: String = String()
    var delegate: UIViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

    func configureCell(_ taxiName: String, taxiNumber: String) {
        
        self.taxiName.text = taxiName
        self.taxiNumberCall = taxiNumber
    }
    
    @IBAction func callTabbed(_ sender: Any) {
        let ac = UIAlertController(title: self.taxiNumberCall, message: nil, preferredStyle: .alert)
        
        ac.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        ac.addAction(UIAlertAction(title: "Call", style: .default, handler: {(UIAlertAction) in
            
            let phoneCallURL:URL = URL(string: "tel://\(self.taxiNumberCall)")!
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.openURL(phoneCallURL);
            } else {
                showErrorAlert("Calling is not supported on this device", msg: "", View: self.delegate!)
            }
        }))
        self.delegate?.present(ac, animated: true, completion: nil)
    }
}

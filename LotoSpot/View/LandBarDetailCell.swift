//
//  LandBarDetailCell.swift
//  LotoSpot
//
//  Created by Debra Rideout on 5/26/16.
//  Copyright © 2016 RapidWare. All rights reserved.
//

import UIKit

class LandBarDetailCell: UITableViewCell {

    @IBOutlet weak var barName: UILabel!
    @IBOutlet weak var checkInCount: UILabel!
    @IBOutlet weak var barMenu: UITextView!
    @IBOutlet weak var barCuopons: UITextView!
    @IBOutlet weak var barDescription: UITextView!
    @IBOutlet weak var barReviews: UITextView!
    @IBOutlet weak var barAddress: UILabel!
    
    var bar: Bar!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(_ bar: Bar) {
        self.bar = bar
        
        self.barName.text = bar.barName
//        self.barMenu.text = bar.barMenu
//        self.barCuopons.text = bar.barCoupons
        self.barDescription.text = bar.barDescription
        self.barAddress.text = bar.barAddress
        
        if bar.barCheckIns != nil {
            self.checkInCount.text = "\(bar.barCheckIns!)"
        } else {
            self.checkInCount.text = "0"
        }
    }

}

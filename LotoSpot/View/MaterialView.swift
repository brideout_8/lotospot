//
//  MaterialView.swift
//  LotoSpot
//
//  Created by Bret Rideout on 5/22/16.
//  Copyright © 2016 RapidWare. All rights reserved.
//

import UIKit

class MaterialView: UIView {

    override func awakeFromNib() {
        // Drawing code
        layer.cornerRadius = 5.0
        layer.shadowColor = UIColor(red: SHADOW_COLOR, green: SHADOW_COLOR, blue: SHADOW_COLOR, alpha: 0.5).cgColor
        layer.shadowOpacity = 0.8
        layer.shadowRadius = 5.0
        layer.shadowOffset = CGSize(width: 0.0, height: 1.5)
    }
}

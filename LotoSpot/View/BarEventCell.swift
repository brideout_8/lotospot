//
//  BarEventCell.swift
//  LotoSpot
//
//  Created by Debra Rideout on 5/28/16.
//  Copyright © 2016 RapidWare. All rights reserved.
//

import UIKit

class BarEventCell: UITableViewCell {

    @IBOutlet weak var barEvent: UILabel!
    @IBOutlet weak var eventTime: UILabel!
    @IBOutlet weak var eventDate: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(_ name: String, time: String, date: String) {
        
        self.barEvent.text = name
        self.eventTime.text = time
        self.eventDate.text = date
        
    }

}

//
//  BarCouponCell.swift
//  LotoSpot
//
//  Created by Debra Rideout on 5/26/16.
//  Copyright © 2016 RapidWare. All rights reserved.
//

import UIKit

class BarCouponCell: UITableViewCell {

    @IBOutlet weak var barCuopons: UILabel!
    
    func configureCell(_ coupons: String) {
        
        self.barCuopons.text = coupons

    }

}

//
//  AddTaxiTableViewController.swift
//  LotoSpot
//
//  Created by Bret Rideout on 3/26/17.
//  Copyright © 2017 RapidWare. All rights reserved.
//

import UIKit

class AddTaxiTableViewController: UITableViewController {
    
    @IBOutlet weak var taxiName: UITextField!
    @IBOutlet weak var taxiNumber: UITextField!
    
    var taxiType: String = String()

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }
    
    @IBAction func addTabbed(_ sender: UIButton!) {
        if taxiName.text == "" || taxiNumber.text == "" {
            showErrorAlert("Error", msg: "Taxi Name and number must be filled in.", View: self)
        } else {
            var taxi: [String: AnyObject] = [:]
            
            taxi = ["name":taxiName.text! as AnyObject, "number":taxiNumber.text! as AnyObject]
            
            if taxiType == "cab" {
                let firebaseTaxi = DataService.ds.REF_CAB_TAXIS.childByAutoId()
                firebaseTaxi.setValue(taxi)
                showErrorAlert("Success", msg: "The taxi has been saved.", View: self)
                self.taxiNumber.text = ""
                self.taxiName.text = ""
            } else if taxiType == "water" {
                let firebaseTaxi = DataService.ds.REF_WATER_TAXIS.childByAutoId()
                firebaseTaxi.setValue(taxi)
                showErrorAlert("Success", msg: "The taxi has been saved", View: self)
                self.taxiName.text = ""
                self.taxiNumber.text = ""
            }
        }
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  TaxiDetailTableViewController.swift
//  LotoSpot
//
//  Created by Bret Rideout on 3/26/17.
//  Copyright © 2017 RapidWare. All rights reserved.
//

import UIKit
import Firebase
import MBProgressHUD

class TaxiDetailTableViewController: UITableViewController {
    
    var taxiType = String()
    var taxiNames: [String] = []
    var taxiNumbers: [String] = []
    var taxiKeys: [String] = []

    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        tableView.estimatedRowHeight = 100.0
        tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.backgroundColor = UIColor.init(red: CGFloat(241/255.0), green: CGFloat(241/255.0), blue: CGFloat(241/255.0), alpha: CGFloat(1.0))
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        if UserDefaults.standard.bool(forKey: KEY_ADMIN) {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.addTapped))
        }
        
        //TODO: Observe from firebase
        let activityIndicator = MBProgressHUD.showAdded(to: self.view, animated: true)
        activityIndicator?.labelText = "Loading Taxis"
        activityIndicator?.isUserInteractionEnabled = false
        UIApplication.shared.beginIgnoringInteractionEvents()
        if taxiType == "cab" {
            self.navigationItem.title = "Cab Taxi"
            DataService.ds.REF_CAB_TAXIS.observe(.value, with: { snapshot in
                self.taxiNames = []
                self.taxiNumbers = []
                if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                    for snap in snapshots {
                        if let taxiDict = snap.value as? Dictionary<String, AnyObject> {
                            self.taxiNames.append(taxiDict["name"] as! String)
                            self.taxiNumbers.append(taxiDict["number"] as! String)
                            self.taxiKeys.append(snap.key)
                        }
                    }
                }
                self.tableView.reloadData()
                UIApplication.shared.endIgnoringInteractionEvents()
                activityIndicator?.hide(true)
                print(self.taxiNames)
            })
        }
        if taxiType == "water" {
            self.navigationItem.title = "Water Taxi"
            DataService.ds.REF_WATER_TAXIS.observe(.value, with: { snapshot in
                self.taxiNames = []
                self.taxiNumbers = []
                if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                    for snap in snapshots {
                        if let taxiDict = snap.value as? Dictionary<String, AnyObject> {
                            self.taxiNames.append(taxiDict["name"] as! String)
                            self.taxiNumbers.append(taxiDict["number"] as! String)
                            self.taxiKeys.append(snap.key)
                        }
                    }
                }
                self.tableView.reloadData()
                UIApplication.shared.endIgnoringInteractionEvents()
                activityIndicator?.hide(true)
            })
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return taxiNames.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let taxiName = taxiNames[indexPath.row]
        let taxiNumber = taxiNumbers[indexPath.row]
        if let cell = tableView.dequeueReusableCell(withIdentifier: CELL_TAXI_DETAIL_CELL) as? TaxiCellTableViewCell {
            cell.configureCell(taxiName, taxiNumber: taxiNumber)
            cell.delegate = self
            return cell
        }
        return TaxiCellTableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
    func addTapped() {
        performSegue(withIdentifier: SEGUE_ADD_TAXI, sender: self)
    }

    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if UserDefaults.standard.bool(forKey: KEY_ADMIN) {
            return true
        }
        return false
    }
 

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if taxiType == "cab" {
                let taxiRef = DataService.ds.REF_CAB_TAXIS.child(taxiKeys[indexPath.row])
                taxiRef.removeValue()
                self.taxiKeys.remove(at: indexPath.row)
                self.taxiNames.remove(at: indexPath.row)
                self.taxiNumbers.remove(at: indexPath.row)
            } else if taxiType == "water" {
                let taxiRef = DataService.ds.REF_WATER_TAXIS.child(taxiKeys[indexPath.row])
                taxiRef.removeValue()
                self.taxiKeys.remove(at: indexPath.row)
                self.taxiNames.remove(at: indexPath.row)
                self.taxiNumbers.remove(at: indexPath.row)
            }
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
 

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SEGUE_ADD_TAXI {
            if let addTaxiVC = segue.destination as? AddTaxiTableViewController {
                addTaxiVC.taxiType = self.taxiType
            }
        }
    }
 

}

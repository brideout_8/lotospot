//
//  LandBarDetailTableViewController.swift
//  LotoSpot
//
//  Created by Debra Rideout on 5/26/16.
//  Copyright © 2016 RapidWare. All rights reserved.
//

import UIKit
import Firebase
import MBProgressHUD
import MapKit
import SafariServices

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class LandBarDetailTableViewController: UITableViewController, MKMapViewDelegate {
    
    @IBOutlet weak var barName: UILabel!
    @IBOutlet weak var checkInCount: UILabel!
    @IBOutlet weak var starOne: UIImageView!
    @IBOutlet weak var starTwo: UIImageView!
    @IBOutlet weak var starThree: UIImageView!
    @IBOutlet weak var starFour: UIImageView!
    @IBOutlet weak var starFive: UIImageView!
    @IBOutlet weak var checkIn: UIButton!
    @IBOutlet weak var barAddress: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var aboutUs: UILabel!
    @IBOutlet weak var barImg: UIImageView!
    
    var barAddressHolder = String()
    var barLon: CLLocationDegrees = CLLocationDegrees()
    var barLat: CLLocationDegrees = CLLocationDegrees()
    let annotation = MKPointAnnotation()
    var barCoordinate = CLLocationCoordinate2D()
    var barType: String = String()
    static var imageCache: NSCache<NSString, UIImage> = NSCache()
    var barRating = 0
    var barStars = [Int]()
    var bars: [Bar] = []
    var bar: Bar!
    var checkIns: Int!
    var checkedIn = false
    var hiddenBar = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.title = self.bar.barName
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundColor = UIColor.init(red: CGFloat(241/255.0), green: CGFloat(241/255.0), blue: CGFloat(241/255.0), alpha: CGFloat(1.0))
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        if (UserDefaults.standard.bool(forKey: KEY_ADMIN)) {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(self.editTapped))
        }
        let activityIndicator = MBProgressHUD.showAdded(to: self.view, animated: true)
        activityIndicator?.labelText = "Loading Bar"
        activityIndicator?.isUserInteractionEnabled = false
        UIApplication.shared.beginIgnoringInteractionEvents()

        if landBar {
            
            UserDefaults.standard.setValue(bar.barKey, forKey: KEY_LAND_BAR_UID)
            DataService.ds.REF_LAND_CHECK_INS.observe(.value, with: { snapshot in
                print("Check ins \(snapshot.value)")
                self.checkIns = 0
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM-dd-yyyy H:m" /*find out and place date format from http://userguide.icu-project.org/formatparse/datetime */
                
                if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                    for snap in snapshots {
                        if let barDict = snap.value as? Dictionary<String, AnyObject> {
                            for (user, check) in barDict {
                                //If Check is less than 2 hours append checkIns
                                let newDate = dateFormatter.date(from: check as! String)
                                var elapsedTime = 0.0
                                if newDate != nil {
                                    elapsedTime = Date().timeIntervalSince(newDate!)
                                    if elapsedTime < 7200 {
                                        self.checkIns = self.checkIns + 1
                                        //If user == user UID then disable check in button
                                        if user == UserDefaults.standard.object(forKey: KEY_UID) as! String {
                                            self.checkedIn = true
                                            self.alreadyCheckedIn()
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                print("Check Ins\(self.bar.barCheckIns)")
                if self.checkIns != nil {
                    self.checkInCount.text = "\(self.checkIns!)"
                } else {
                    self.checkInCount.text = "0"
                }
                self.tableView.reloadData()
                UIApplication.shared.endIgnoringInteractionEvents()
                activityIndicator?.hide(true)
                
            })
        } else {
            UserDefaults.standard.setValue(bar.barKey, forKey: KEY_WATER_BAR_UID)
            DataService.ds.REF_WATER_CHECK_INS.observe(.value, with: { snapshot in

                self.checkIns = 0
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM-dd-yyyy H:m" /*find out and place date format from http://userguide.icu-project.org/formatparse/datetime */
                
                if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                    for snap in snapshots {
                        if let barDict = snap.value as? Dictionary<String, AnyObject> {
                            for (user, check) in barDict {
                                //If Check is less than 2 hours append checkIns
                                let newDate = dateFormatter.date(from: check as! String)
                                var elapsedTime = 0.0
                                if newDate != nil {
                                    elapsedTime = Date().timeIntervalSince(newDate!)
                                    if elapsedTime < 7200 {
                                        self.checkIns = self.checkIns + 1
                                        //If user == user UID then disable check in button
                                        if user == UserDefaults.standard.object(forKey: KEY_UID) as! String {
                                            self.checkedIn = true
                                            self.alreadyCheckedIn()
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if self.checkIns != nil {
                    self.checkInCount.text = "\(self.checkIns!)"
                } else {
                    self.checkInCount.text = "0"
                }
                self.tableView.reloadData()
                UIApplication.shared.endIgnoringInteractionEvents()
                activityIndicator?.hide(true)
            })
        }
        self.aboutUs.text = bar.barDescription
        self.barName.text = bar.barName
        self.barLon = Double(bar.barLon!)!
        self.barLat = Double(bar.barLat!)!
        self.barAddress.text = bar.barAddress
        self.barCoordinate = CLLocationCoordinate2DMake(barLat, barLon)
        self.annotation.coordinate = barCoordinate
        self.mapView.addAnnotation(annotation)
        self.mapView.region = MKCoordinateRegionMakeWithDistance(
            barCoordinate, 500, 500)
        if let barImageURL = bar.barImage {
            if let imgCache = LandBarDetailTableViewController.imageCache.object(forKey: barImageURL as NSString) {
                self.barImg.image = imgCache
            } else {
                let ref = FIRStorage.storage().reference(forURL: barImageURL as String)
                ref.data(withMaxSize: 2 * 1024 * 1024, completion: { (data, error) in
                    if error != nil {
                        print("error downloading photo")
                        print(error.debugDescription)
                    } else {
                        print("Downloaded")
                        if let imageData = data {
                            if let img = UIImage(data: imageData) {
                                self.barImg.image = img
                                LandBarDetailTableViewController.imageCache.setObject(img, forKey: barImageURL as NSString)
                            }
                        }
                    }
                })
            }
        }
        
        if bar.barStars?.count > 0 {
            for star in bar.barStars! {
                if star == 6 {
                    barRating = barRating + 1
                } else {
                    barRating = barRating + star
                }
            }
            barRating = barRating / bar.barStars!.count
        } else {
            barRating = 0
        }
        
        
        if barRating == 0 {
            starOne.image = UIImage(named: "Star Empty")
            starTwo.image = UIImage(named: "Star Empty")
            starThree.image = UIImage(named: "Star Empty")
            starFour.image = UIImage(named: "Star Empty")
            starFive.image = UIImage(named: "Star Empty")
        } else if barRating == 1 {
            starOne.image = UIImage(named: "Star Full")
            starTwo.image = UIImage(named: "Star Empty")
            starThree.image = UIImage(named: "Star Empty")
            starFour.image = UIImage(named: "Star Empty")
            starFive.image = UIImage(named: "Star Empty")
        } else if barRating <= 2 {
            starOne.image = UIImage(named: "Star Full")
            starTwo.image = UIImage(named: "Star Full")
            starThree.image = UIImage(named: "Star Empty")
            starFour.image = UIImage(named: "Star Empty")
            starFive.image = UIImage(named: "Star Empty")
        } else if barRating <= 3 {
            starOne.image = UIImage(named: "Star Full")
            starTwo.image = UIImage(named: "Star Full")
            starThree.image = UIImage(named: "Star Full")
            starFour.image = UIImage(named: "Star Empty")
            starFive.image = UIImage(named: "Star Empty")
        } else if barRating <= 4 {
            starOne.image = UIImage(named: "Star Full")
            starTwo.image = UIImage(named: "Star Full")
            starThree.image = UIImage(named: "Star Full")
            starFour.image = UIImage(named: "Star Full")
            starFive.image = UIImage(named: "Star Empty")
        } else if barRating > 4 {
            starOne.image = UIImage(named: "Star Full")
            starTwo.image = UIImage(named: "Star Full")
            starThree.image = UIImage(named: "Star Full")
            starFour.image = UIImage(named: "Star Full")
            starFive.image = UIImage(named: "Star Full")
        } else {
            starOne.image = UIImage(named: "Star Empty")
            starTwo.image = UIImage(named: "Star Empty")
            starThree.image = UIImage(named: "Star Empty")
            starFour.image = UIImage(named: "Star Empty")
            starFive.image = UIImage(named: "Star Empty")
        }
    }
    
    @IBAction func directions(_ sender: UIButton!) {
        let coordinate = CLLocationCoordinate2DMake(barLat,barLon)
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
        mapItem.name = bar.barName
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
    }
    
    @IBAction func editTapped(_ sender: UIButton!) {
        performSegue(withIdentifier: SEGUE_EDIT_BAR, sender: self)
    }

    @IBAction func callUs(_ sender: AnyObject) {
        
        let ac = UIAlertController(title: bar.barPhoneNumber, message: nil, preferredStyle: .alert)
        
        ac.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        ac.addAction(UIAlertAction(title: "Call", style: .default, handler: {(UIAlertAction) in
            
            let phoneCallURL:URL = URL(string: "tel://\(self.bar.barPhoneNumber)")!
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.openURL(phoneCallURL);
            } else {
                showErrorAlert("Calling is not supported on this device", msg: "", View: self)
            }
        }))
        self.present(ac, animated: true)
    }
    
    @IBAction func gotToSite(_ sender: AnyObject) {
        if let checkURL = URL(string: bar.barWebsite) {
            let vc = SFSafariViewController(url: checkURL, entersReaderIfAvailable: true)
            present(vc, animated: true)
        }
    }

    @IBAction func goToMenu(_ sender: AnyObject) {
        print("Menu")
        if let checkURL = URL(string: bar.barMenuSite) {
            let vc = SFSafariViewController(url: checkURL, entersReaderIfAvailable: true)
            present(vc, animated: true)
        }
    }
    
    @IBAction func checkIn(_ sender: UIButton) {
        let date = Date()
        var checkIn: Dictionary<String, AnyObject> = [:]
        let user = UserDefaults.standard.value(forKey: KEY_UID) as! String
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy HH:mm"
        let newDate = dateFormatter.string(from: date)
        print(newDate)
        checkIn = [user:newDate as AnyObject]

        if landBar {
            let firebaseCheckIn = DataService.ds.REF_LAND_CHECK_INS.childByAutoId()
            firebaseCheckIn.setValue(checkIn)
        } else {
            let firebaseCheckIn = DataService.ds.REF_WATER_CHECK_INS.childByAutoId()
            firebaseCheckIn.setValue(checkIn)
        }
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        if landBar {
            return 11
        }
        return 12
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 1 {
            if checkedIn {
                self.tableView.reloadData()
//                self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
                
            }
        }
        if indexPath.row == 10 {
            
            if UserDefaults.standard.bool(forKey: KEY_ADMIN) {
                performSegue(withIdentifier: SEGUE_BAR_EVENTS, sender: self)
                
            } else if bar.barEventDate?.count < 1 || bar.barEventTime?.count < 1 || bar.barEventName?.count < 1 {
                showErrorAlert("", msg: "No Events Scheduled At This Time", View: self)
                self.tableView.reloadData()
                
            } else if checkedIn == false {
                showErrorAlert("", msg: "You must check in to see events.", View: self)
                self.tableView.reloadData()
            } else {
                performSegue(withIdentifier: SEGUE_BAR_EVENTS, sender: self)
            }
        }
        if indexPath.row == 5 {
            let ac = UIAlertController(title: bar.barPhoneNumber, message: nil, preferredStyle: .alert)
            
            ac.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            ac.addAction(UIAlertAction(title: "Call", style: .default, handler: {(UIAlertAction) in
                
                let phoneCallURL:URL = URL(string: "tel://\(self.bar.barPhoneNumber)")!
                let application:UIApplication = UIApplication.shared
                if (application.canOpenURL(phoneCallURL)) {
                    application.openURL(phoneCallURL);
                } else {
                    showErrorAlert("Calling is not supported on this device", msg: "", View: self)
                }
            }))
            self.present(ac, animated: true)
        }
        if indexPath.row == 6 {
            if let checkURL = URL(string: bar.barWebsite) {
                let vc = SFSafariViewController(url: checkURL, entersReaderIfAvailable: true)
                present(vc, animated: true)
            }
        }
        if indexPath.row == 7 {
            if let checkURL = URL(string: bar.barMenuSite) {
                let vc = SFSafariViewController(url: checkURL, entersReaderIfAvailable: true)
                present(vc, animated: true)
            }
        }
        if indexPath.row == 8 {
            if UserDefaults.standard.bool(forKey: KEY_ADMIN) {
                performSegue(withIdentifier: SEGUE_BAR_COUPONS, sender: self)
                
            } else if bar.barCoupons?.count < 1 {
                showErrorAlert("", msg: "No Coupons At This Time", View: self)
                self.tableView.reloadData()
            } else if checkedIn == false {
                showErrorAlert("", msg: "You must check in to see coupons", View: self)
                self.tableView.reloadData()
            } else {
                performSegue(withIdentifier: SEGUE_BAR_COUPONS, sender: self)
            }
        }
        if indexPath.row == 9 {
            performSegue(withIdentifier: SEGUE_BAR_REVIEWS, sender: self)
        }
        if indexPath.row == 11 {
            performSegue(withIdentifier: SEGUE_BAR_PARKING, sender: self)
        }
        
//        if indexPath.row == 2 {
//            performSegue(withIdentifier: SEGUE_BAR_MAP, sender: self)
//        }
        
//        if indexPath.row == 3 {
//            performSegue(withIdentifier: SEGUE_BAR_ABOUT_US, sender: self)
//        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
    // MARK: - Helper Functions
 
    func alreadyCheckedIn() {
        if self.checkedIn {
            self.checkIn.isEnabled = false
            self.checkIn.setTitleColor(UIColor.red, for: .disabled)
            self.checkIn.setTitle("Checked In", for: .disabled)
            self.checkIn.backgroundColor = UIColor.gray
        }
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SEGUE_BAR_MENU {
            if let destination = segue.destination as? BarMenuTableViewController {
//                let indexPath = tableView.indexPathForSelectedRow!.row
                destination.barMenuItems = bar.barMenu!
            }
        }
        
        if segue.identifier == SEGUE_BAR_COUPONS {
            print("Segue")  
            if let destination = segue.destination as? BarCouponsTableViewController {
                destination.barCoupons = bar.barCouponsUser
                destination.barKey = bar.barKey
                destination.barType = self.barType
                destination.bar = self.bar
            }
        }
        
        if segue.identifier == SEGUE_BAR_EVENTS {
            if let destination = segue.destination as? BarEventsTableViewController {
                print("thisssss\(bar.barEventDate)")
                if bar.barEventDate != nil {
                    destination.eventName = bar.barEventName!
                    destination.eventTime = bar.barEventTime!
                    destination.eventDate = bar.barEventDate!
                    destination.barKey = bar.barKey
                    destination.bar = self.bar
                    destination.barType = self.barType
                }
            }
        }
        
        if segue.identifier == SEGUE_EDIT_BAR {
            if let destination = segue.destination as? EditBarTableViewController {
                destination.barKey = bar.barKey
                destination.bars = bars
                destination.bar = bar
                destination.barType = self.barType
                destination.hiddenBar = self.hiddenBar
            }
        }
        
        if segue.identifier == SEGUE_BAR_REVIEWS {
            if let destination = segue.destination as? BarReviewsViewController {
                if bar.barStars?.count > 0 {
                        destination.starsOfBar = bar.barStars!
                }
                if bar.barReview?.count > 0 {
                    destination.reviewsOfBar = bar.barReview!
                }
                if bar.barReviewUser?.count > 0 {
                    destination.namesOfReview = bar.barReviewUser!
                }
            }
        }
        
        if segue.identifier == SEGUE_BAR_PARKING {
            if let destination = segue.destination as? BoatSlipsTableViewController {
                if bar.barParkingUncovered != nil {
                    destination.uncoveredParking = bar.barParkingUncovered!
                }
                if bar.barParkingCovered != nil {
                    destination.coveredParking = bar.barParkingCovered!
                }
                if bar.barParkingLength != nil {
                    destination.lengthOfParking = bar.barParkingLength!
                }
            }
        }
        
        if segue.identifier == SEGUE_BAR_ABOUT_US {
            if let destination = segue.destination as? BarAboutUsTableViewController {
//                destination.aboutUsHolder = bar.barDescription
                destination.mondayTimeHolder = bar.barMonday
                destination.tuesdayTimeHolder = bar.barTuesday
                destination.wednesdayTimeHolder = bar.barWednesday
                destination.thursdayTimeHolder = bar.barThursday
                destination.fridayTimeHolder = bar.barFriday
                destination.saturdayTimeHolder = bar.barSaturday
                destination.sundayTimeHolder = bar.barSunday
            }
        }
        
        if segue.identifier == SEGUE_BAR_MAP {
            if let destination = segue.destination as? BarMapTableViewController {
                destination.barAddressHolder = bar.barAddress
                destination.barLatHolder = bar.barLat!
                destination.barLonHolder = bar.barLon!
                destination.barName = bar.barName
                
            }
        }
        
        if segue.identifier == SEGUE_BAR_COUPONS_ADD {
            if let destination = segue.destination as? AddCouponTableViewController {
                destination.barKey = bar.barKey
            }
        }
        
        if segue.identifier == SEGUE_BAR_MENU_ADD {
            if let destination = segue.destination as? AddMenuItemTableViewController {
                destination.barKey = bar.barKey
            }
        }
        
        if segue.identifier == SEGUE_BAR_EVENTS_ADD {
            if let destination = segue.destination as? AddEventTableViewController {
                destination.barKey = bar.barKey
            }
        }
    }
}

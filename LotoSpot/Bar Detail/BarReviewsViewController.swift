//
//  BarReviewsViewController.swift
//  LotoSpot
//
//  Created by Debra Rideout on 5/28/16.
//  Copyright © 2016 RapidWare. All rights reserved.
//

import UIKit
import Firebase

class BarReviewsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var starOne: UIImageView!
    @IBOutlet weak var starTwo: UIImageView!
    @IBOutlet weak var starThree: UIImageView!
    @IBOutlet weak var starFour: UIImageView!
    @IBOutlet weak var starFive: UIImageView!
    @IBOutlet weak var reviewText: UITextField!
    @IBOutlet weak var nameOfReviewer: UILabel!
    
    var starsOfBar = [Int]()
    var reviewsOfBar = [String]()
    var namesOfReview = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(BarReviewsViewController.handleTap(_:))))
        
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        if landBar {
            DataService.ds.REF_LAND_BARS_REVIEWS.observe(.value, with: { snapshot in
             //   print(snapshot.value)
                
                self.starsOfBar = []
                self.reviewsOfBar = []
                self.namesOfReview = []
                
                if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                    
                    for snap in snapshots {
                  //      print("SNAP: \(snap)")
                        
                        if let barDict = snap.value as? Dictionary<String, AnyObject> {
                            for (user, review) in barDict {
                                self.namesOfReview.insert(user, at: 0)
                                let dict2 = review as! [String: String]
                                for (star, review2) in dict2 {
                                    self.reviewsOfBar.insert(review2, at: 0)
                                    self.starsOfBar.insert(Int(star)!, at: 0)
                                }
                            }
                        }
                    }
                }
                
                self.tableView.reloadData()
                
            })
            
        } else {
            DataService.ds.REF_WATER_BARS_REVIEWS.observe(.value, with: { snapshot in
                print(snapshot.value)
                
                self.starsOfBar = []
                self.reviewsOfBar = []
                self.namesOfReview = []
                
                if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                    
                    for snap in snapshots {
                        print("SNAP: \(snap)")
                        
                        if let barDict = snap.value as? Dictionary<String, AnyObject> {
                            for (user, review) in barDict {
                                self.namesOfReview.insert(user, at: 0)
                                let dict2 = review as! [String: String]
                                for (star, review2) in dict2 {
                                    self.reviewsOfBar.insert(review2, at: 0)
                                    self.starsOfBar.insert(Int(star)!, at: 0)
                                }
                            }
                        }
                    }
                }
                
                self.tableView.reloadData()
                
            })
        }

        // Do any additional setup after loading the view.
    }
    
    func handleTap(_ sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            view.endEditing(true)
        }
        sender.cancelsTouchesInView = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - TableView Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return starsOfBar.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let stars = starsOfBar[indexPath.row]
        let review = reviewsOfBar[indexPath.row]
        let name = namesOfReview[indexPath.row]
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: CELL_BAR_REVIEW_CELL) as? BarReviewCell {
            
            cell.configureCell(review, stars: stars, name: name)
            return cell
        } else {
            return BarReviewCell()
        }
    }
    @IBAction func submitTabbed(_ sender: UIButton) {
        
        if namesOfReview.contains(UserDefaults.standard.value(forKey: KEY_UID) as! String) {
            showErrorAlert("", msg: "Only one review per user", View: self)
        } else {
            if UserDefaults.standard.value(forKey: "userName") != nil {
                let user = UserDefaults.standard.value(forKey: KEY_UID) as! String
                if starOne.image == UIImage(named: "Star Empty") {
                    showErrorAlert("Error", msg: "Please select how many stars you want to rate the bar.", View: self)
                } else if reviewText.text == "" {
                    showErrorAlert("Error", msg: "Please explain your rating.", View: self)
                } else {
                    var review: [String: [String: String]] = [:]
                    
                    if starFive.image == UIImage(named: "Star Full") {
                        review = [
                            user:["5":reviewText.text!]
                        ]
                        
                    } else if starFour.image == UIImage(named: "Star Full") {
                        review = [
                            user:["4":reviewText.text!]
                        ]
                    } else if starThree.image == UIImage(named: "Star Full") {
                        review = [
                            user:["3":reviewText.text!]
                        ]
                    } else if starTwo.image == UIImage(named: "Star Full") {
                        review = [
                            user:["2":reviewText.text!]
                        ]
                    } else {
                        review = [
                            user:["6":reviewText.text!]
                        ]
                    }
                    //Saving to firebase
                    print(review)
                    if landBar {
                        let firebaseReview = DataService.ds.REF_LAND_BARS_REVIEWS.childByAutoId()
                        firebaseReview.setValue(review)
                    } else {
                        let firebaseReview = DataService.ds.REF_WATER_BARS_REVIEWS.childByAutoId()
                        firebaseReview.setValue(review)
                    }
                    
                    tableView.reloadData()
                    reviewText.text = ""
                    starOne.image = UIImage(named: "Star Empty")
                    starTwo.image = UIImage(named: "Star Empty")
                    starThree.image = UIImage(named: "Star Empty")
                    starFour.image = UIImage(named: "Star Empty")
                    starFive.image = UIImage(named: "Star Empty")
                    //reload table
                }
            } else {
                showErrorAlert("", msg: "Please enter a valid name", View: self)
            }
        }
    }

    @IBAction func firstStar(_ sender: UITapGestureRecognizer) {
        if starOne.image == UIImage(named: "Star Empty") {
            starOne.image = UIImage(named: "Star Full")
        } else if starTwo.image == UIImage(named: "Star Full") {
            starTwo.image = UIImage(named: "Star Empty")
            starThree.image = UIImage(named: "Star Empty")
            starFour.image = UIImage(named: "Star Empty")
            starFive.image = UIImage(named: "Star Empty")
        } else {
            starOne.image = UIImage(named: "Star Empty")
        }
    }
    
    @IBAction func secondStar(_ sender: UITapGestureRecognizer) {
        if starTwo.image == UIImage(named: "Star Empty") {
            starTwo.image = UIImage(named: "Star Full")
            starOne.image = UIImage(named: "Star Full")
        } else if starThree.image == UIImage(named: "Star Full") {
            starThree.image = UIImage(named: "Star Empty")
            starFour.image = UIImage(named: "Star Empty")
            starFive.image = UIImage(named: "Star Empty")
        } else {
            starTwo.image = UIImage(named: "Star Empty")
        }
    }
    
    @IBAction func thirdStar(_ sender: UITapGestureRecognizer) {
        if starThree.image == UIImage(named: "Star Empty") {
            starThree.image = UIImage(named: "Star Full")
            starTwo.image = UIImage(named: "Star Full")
            starOne.image = UIImage(named: "Star Full")
        } else if starFour.image == UIImage(named: "Star Full") {
            starFour.image = UIImage(named: "Star Empty")
            starFive.image = UIImage(named: "Star Empty")
        } else {
            starThree.image = UIImage(named: "Star Empty")
        }
    }
    
    @IBAction func fourthStar(_ sender: UITapGestureRecognizer) {
        if starFour.image == UIImage(named: "Star Empty") {
            starFour.image = UIImage(named: "Star Full")
            starThree.image = UIImage(named: "Star Full")
            starTwo.image = UIImage(named: "Star Full")
            starOne.image = UIImage(named: "Star Full")
        } else if starFive.image == UIImage(named: "Star Full") {
            starFive.image = UIImage(named: "Star Empty")
        } else {
            starFour.image = UIImage(named: "Star Empty")
        }
    }
    
    @IBAction func fifthStar(_ sender: UITapGestureRecognizer) {
        if starFive.image == UIImage(named: "Star Empty") {
            starFive.image = UIImage(named: "Star Full")
            starFour.image = UIImage(named: "Star Full")
            starThree.image = UIImage(named: "Star Full")
            starTwo.image = UIImage(named: "Star Full")
            starOne.image = UIImage(named: "Star Full")
        } else {
            starFive.image = UIImage(named: "Star Empty")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

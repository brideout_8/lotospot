//
//  BarEventsTableViewController.swift
//  LotoSpot
//
//  Created by Debra Rideout on 5/28/16.
//  Copyright © 2016 RapidWare. All rights reserved.
//

import UIKit

class BarEventsTableViewController: UITableViewController {
    
//    var barEvents = [String:AnyObject]()
    var eventName = [String]()
    var eventTime = [String]()
    var eventDate = [String]()
    var barKey = String()
    var barType = String()
    var bar: Bar!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 140.0
        tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.backgroundColor = UIColor.init(red: CGFloat(241/255.0), green: CGFloat(241/255.0), blue: CGFloat(241/255.0), alpha: CGFloat(1.0))
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        if UserDefaults.standard.bool(forKey: KEY_ADMIN) {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.addTapped))
        }

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addTapped() {
        performSegue(withIdentifier: SEGUE_BAR_EVENTS_ADD2, sender: self)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return eventName.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let name = eventName[indexPath.row]
        let time = eventTime[indexPath.row]
        let date = eventDate[indexPath.row]
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: CELL_BAR_EVENT_CELL) as? BarEventCell {
            
            cell.configureCell(name, time: time, date: date)
            return cell
        } else {
            return BarEventCell()
        }
    }
    

    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if UserDefaults.standard.bool(forKey: KEY_ADMIN) {
            return true
        }
        return false
    }
 
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }

 
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if barType == "water" {
                let eventRef = DataService.ds.REF_WATER_BARS.child(barKey).child("events").child(bar.barEventKey![indexPath.row])
                eventRef.removeValue()
                eventName.remove(at: indexPath.row)
                eventTime.remove(at: indexPath.row)
                eventDate.remove(at: indexPath.row)
            } else {
                let eventRef = DataService.ds.REF_LAND_BARS.child(barKey).child("events").child(bar.barEventKey![indexPath.row])
                eventRef.removeValue()
                eventName.remove(at: indexPath.row)
                eventTime.remove(at: indexPath.row)
                eventDate.remove(at: indexPath.row)
            }
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
 

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? AddEventTableViewController {
            destination.barKey = barKey
        }
    }

}

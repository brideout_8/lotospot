//
//  BarCouponsTableViewController.swift
//  LotoSpot
//
//  Created by Debra Rideout on 5/26/16.
//  Copyright © 2016 RapidWare. All rights reserved.
//

import UIKit

class BarCouponsTableViewController: UITableViewController {
    
    var barCoupons: [String] = []
    var bar: Bar!
    var barKey = String()
    var barType = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundColor = UIColor.init(red: CGFloat(241/255.0), green: CGFloat(241/255.0), blue: CGFloat(241/255.0), alpha: CGFloat(1.0))
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        if UserDefaults.standard.bool(forKey: KEY_ADMIN) {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.addTapped))
        }
    }
    
    func addTapped() {
        performSegue(withIdentifier: SEGUE_BAR_COUPONS_ADD2, sender: self)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return barCoupons.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let coupon = barCoupons[indexPath.row]
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: CELL_BAR_COUPONS_CELL) as? BarCouponCell {
            print("Cell")
            cell.configureCell(coupon)
            return cell
        } else {
            return BarCouponCell()
        }

    }
    

    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if UserDefaults.standard.bool(forKey: KEY_ADMIN) {
            return true
        }
        return false
    }

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if barType == "water" {
                let couponRef = DataService.ds.REF_WATER_BARS.child(barKey).child("coupon").child(bar.barCouponsKeys![indexPath.row])
                couponRef.removeValue()
                barCoupons.remove(at: indexPath.row)
            } else {
                let couponRef = DataService.ds.REF_LAND_BARS.child(barKey).child("coupon").child(bar.barCouponsKeys![indexPath.row])
                couponRef.removeValue()
                barCoupons.remove(at: indexPath.row)
            }
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? AddCouponTableViewController {
            destination.barKey = barKey
        }
    }
}

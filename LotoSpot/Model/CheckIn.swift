//
//  CheckIn.swift
//  LotoSpot
//
//  Created by Debra Rideout on 5/25/16.
//  Copyright © 2016 RapidWare. All rights reserved.
//

import Foundation
import UIKit

class CheckIns {
    fileprivate var _CheckIns: Int!
    
    var checkIns: Int {
        return _CheckIns
    }
    
    init(checkIns: Int) {
        self._CheckIns = checkIns
    }
    
    init(postKey: String, dictionary: Dictionary<String, AnyObject>) {
        
        if let checkInsOfBar = dictionary["checkIns"] as? Int {
            self._CheckIns = checkInsOfBar
        }
    }
}



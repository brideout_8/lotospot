//
//  Bar.swift
//  LotoSpot
//
//  Created by Debra Rideout on 5/24/16.
//  Copyright © 2016 RapidWare. All rights reserved.
//

import Foundation
import UIKit
import MapKit


class Bar {
    fileprivate var _barName: String!
    fileprivate var _barMenu: Array<String>?
    fileprivate var _barCoupons: Array<String>?
    fileprivate var _barCouponsKey: [String]?
    fileprivate var _barAddress: String!
    fileprivate var _barCheckIns: [String]?
    fileprivate var _barkCheckInUsers: [String]?
    fileprivate var _barDescription: String!
    fileprivate var _barEvents: [String:AnyObject]?
    fileprivate var _barEventTime: [String]?
    fileprivate var _barEventName: [String]?
    fileprivate var _barEventDate: [String]?
    fileprivate var _barEventKey: [String]?
    fileprivate var _postKey: String!
    fileprivate var _barReview: Array<String>?
    fileprivate var _barStars: [Int]?
    fileprivate var _barParkingUncovered: Int?
    fileprivate var _barParkingCovered: Int?
    fileprivate var _barParkingLength: String?
    fileprivate var _barMonday: String!
    fileprivate var _barTuesday: String!
    fileprivate var _barWednesday: String!
    fileprivate var _barThursday: String!
    fileprivate var _barFriday: String!
    fileprivate var _barSaturday: String!
    fileprivate var _barSunday: String!
    fileprivate var _barReviewUser: [String]?
    fileprivate var _barLon: String?
    fileprivate var _barLat: String?
    fileprivate var _barWebsite: String!
    fileprivate var _barMenuSite: String!
    fileprivate var _barPhoneNumber: String!
    fileprivate var _barImage: NSString!
    
    var checkIn = [String]()
    var barMenuItems = [String]()
    var barCouponsUser = [String]()
    var barCouponsKeyUser = [String]()
    var reviewsOfBar = [String]()
    var eventOfBar = [[String:AnyObject]]()
    var timeOfEvent = [String]()
    var nameOfEvent = [String]()
    var dateOfEvent = [String]()
    var keyOfEvent = [String]()
    var starsOfBar = [Int]()
    var checkInUser = [String]()
    var UserOfReview = [String]()
    
    var barName: String {
        return _barName
    }
    
    var barMenu: Array<String>? {
        return _barMenu
    }
    
    var barCoupons: Array<String>? {
        return _barCoupons
    }
    
    var barCouponsKeys: [String]? {
        return _barCouponsKey
    }
    
    var barAddress: String {
        return _barAddress
    }
    
    var barWebsite: String {
        return _barWebsite
    }
    
    var barMenuSite: String {
        return _barMenuSite
    }
    
    var barPhoneNumber: String {
        return _barPhoneNumber
    }
    
    var barLon: String? {
        return _barLon
    }
    
    var barLat: String? {
        return _barLat
    }
    
    var barCheckIns: [String]? {
        return _barCheckIns
    }
    
    var barCheckInUsers: [String]? {
        return _barkCheckInUsers
    }
    
    var barDescription: String {
        return _barDescription
    }
    
    var barReviewUser: [String]? {
        return _barReviewUser
    }
    
    var barMonday: String {
        return _barMonday
    }
    
    var barTuesday: String {
        return _barTuesday
    }
    
    var barWednesday: String {
        return _barWednesday
    }
    
    var barThursday: String {
        return _barThursday
    }
    
    var barFriday: String {
        return _barFriday
    }
    
    var barSaturday: String {
        return _barSaturday
    }
    
    var barSunday: String {
        return _barSunday
    }
    
    var barParkingCovered: Int? {
        return _barParkingCovered
    }
    
    var barParkingUncovered: Int? {
        return _barParkingUncovered
    }
    
    var barParkingLength: String? {
        return _barParkingLength
    }
    
    var barReview: Array<String>? {
        return _barReview
    }
    
    var barEvents: [String:AnyObject]? {
        return _barEvents
    }
    
    var barEventName: [String]? {
        return _barEventName
    }
    
    var barEventTime: [String]? {
        return _barEventTime
    }
    
    var barEventDate: [String]? {
        return _barEventDate
    }
    
    var barEventKey: [String]? {
        return _barEventKey
    }
    
    var barKey: String {
        return _postKey
    }
    
    var barStars: [Int]? {
        return _barStars
    }
    
    var barImage: NSString? {
        return _barImage
    }
    
    init(description: String, name: String, address: String) {
        self._barDescription = description
        self._barName = name
        self._barAddress = address
    }
    
    init(postKey: String, dictionary: [String:AnyObject]) {
        self._postKey = postKey
        
        if let nameOfBar = dictionary["name"] as? String {
            self._barName = nameOfBar
        }
        
        if let menuOfBar = dictionary["menu"] as? [String:AnyObject] {
            
            for (_, value) in menuOfBar {
                let menuItem = value["item"] as! String
                barMenuItems.append(menuItem)
            }
            self._barMenu = barMenuItems
        }
        
        if let websiteOfBar = dictionary["website"] as? String {
          //  let site = websiteOfBar["site"] as! String
            
            self._barWebsite = websiteOfBar
        }
        
        if let menuSiteOfBar = dictionary["menuSite"] as? String {
            //let site = menuSiteOfBar["menuSite"] as! String
            
            self._barMenuSite = menuSiteOfBar
        }
        
        if let phoneNumberOfBar = dictionary["phoneNumber"] as? String {
           // let number = phoneNumberOfBar["phoneNumber"] as! String
            
            self._barPhoneNumber = phoneNumberOfBar
        }
        
        if let couponOfBar = dictionary["coupon"] as? [String:AnyObject] {
            
            for (key, value) in couponOfBar {
                let coupon = value["coupon"] as! String
                barCouponsUser.append(coupon)
                barCouponsKeyUser.append(key)
            }
            self._barCoupons = barCouponsUser
            self._barCouponsKey = barCouponsKeyUser
        }
        
        if let coorOfBar = dictionary["coordinates"] as? [String:AnyObject] {
            let lat = coorOfBar["lat"] as! String
            let lon = coorOfBar["lon"] as! String
            
            self._barLat = lat
            self._barLon = lon
        }
        
        if let addressOfBar = dictionary["address"] as? String {
            self._barAddress = addressOfBar
        }
        
        if let imageOfBar = dictionary["imageURL"] as? NSString {
            self._barImage = imageOfBar
        }
        
        if let checkInsOfBar = dictionary["checkIns"] as? [String:AnyObject] {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM-dd-yyyy H:m" /*find out and place date format from http://userguide.icu-project.org/formatparse/datetime */
            
                for (_, value) in checkInsOfBar {
                    if let dict = value as? [String: String] {
                        for (user, check) in dict {
                            //If Check is less than 2 hours append checkIns
                            if check == "" {
                                
                            } else {
                                let newDate = dateFormatter.date(from: check)
                                let elapsedTime = Date().timeIntervalSince(newDate!)
                                if elapsedTime < 7200 {
                                    checkIn.append(check)
                                    checkInUser.append(user)
                                }
                            }
                        }
                    }
                }
            
            self._barCheckIns = checkIn
            self._barkCheckInUsers = checkInUser
        }
    
        if let descriptionOfBar = dictionary["desc"] as? String {
            self._barDescription = descriptionOfBar
        }
        
        if let parkingOfBar = dictionary["parking"] as? [String: AnyObject] {
            
            let uncoveredParking = parkingOfBar["uncovered"] as! String
            let coveredParking = parkingOfBar["covered"] as! String
            let length = parkingOfBar["length"] as! String
            
            let uncoveredParkingNum = Int(uncoveredParking)
            let coveredParkingNum = Int(coveredParking)
            
            self._barParkingLength = length
            self._barParkingUncovered = uncoveredParkingNum
            self._barParkingCovered = coveredParkingNum
        }
        
        if let scheduleOfBar = dictionary["hours"] as? [String: AnyObject] {
            
            let monday = scheduleOfBar["monday"] as! String
            let tuesday = scheduleOfBar["tuesday"] as! String
            let wednesday = scheduleOfBar["wednesday"] as! String
            let thursday = scheduleOfBar["thursday"] as! String
            let friday = scheduleOfBar["friday"] as! String
            let saturday = scheduleOfBar["saturday"] as! String
            let sunday = scheduleOfBar["sunday"] as! String
            
            self._barMonday = monday
            self._barTuesday = tuesday
            self._barWednesday = wednesday
            self._barThursday = thursday
            self._barFriday = friday
            self._barSaturday = saturday
            self._barSunday = sunday
        }
        
        if let reviewOfBar = dictionary["reviews"] as? [String:AnyObject] {
            for (_, value) in reviewOfBar {
                let dict = value as! [String: AnyObject]
                for (user, review) in dict {
                    UserOfReview.append(user)
                    let dict2 = review as! [String: String]
                    for (star, review2) in dict2 {
                        starsOfBar.append(Int(star)!)
                        reviewsOfBar.append(review2)
                    }
                }
            }
            self._barReview = reviewsOfBar
            self._barStars = starsOfBar
            self._barReviewUser = UserOfReview
        }
        
        if let eventsOfBar = dictionary["events"] as? [String:AnyObject] {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM-dd-yyyy" /*find out and place date format from http://userguide.icu-project.org/formatparse/datetime */
            
         /*   let newDate = dateFormatter.dateFromString(check)
            let elapsedTime = NSDate().timeIntervalSinceDate(newDate!)
            if elapsedTime < 7200 {
                checkIn.append(check)
                checkInUser.append(user)*/
           /* print("Events\(eventsOfBar)")
            let time = eventsOfBar["time"] as! String
            let date = eventsOfBar["date"] as! String
            let name = eventsOfBar["name"] as! String
            let newDate = dateFormatter.dateFromString(date)
            let elapsedTime = NSDate().timeIntervalSinceDate(newDate!)
            if elapsedTime < 86400 && elapsedTime >= 0 {
                timeOfEvent.append(time)
                dateOfEvent.append(date)
                nameOfEvent.append(name)
            }*/
            
            for (key, value) in  eventsOfBar {
                //nameOfEvent.append(key)
                let time = value["time"] as! String
                let date = value["date"] as! String
                let name = value["name"] as! String
                let newDate = dateFormatter.date(from: date)
                let elapsedTime = Date().timeIntervalSince(newDate!)
                if elapsedTime > -604800 && elapsedTime < 86400 { //86400 is one day
                    timeOfEvent.append(time)
                    dateOfEvent.append(date)
                    nameOfEvent.append(name)
                    keyOfEvent.append(key)
                }
              /*  let dict = value as! [String: String]
                for (date, time) in dict {
                    let newDate = dateFormatter.dateFromString(date)
                    let elapsedTime = NSDate().timeIntervalSinceDate(newDate!)
                    if elapsedTime < 86400 {
                        
                    }
                    timeOfEvent.append(time)
                    dateOfEvent.append(date)
                } */
            }
            self._barEventTime = timeOfEvent
            self._barEventName = nameOfEvent
            self._barEventDate = dateOfEvent
            self._barEventKey = keyOfEvent
         //   self._barEvents = eventsOfBar
        }
    }
}


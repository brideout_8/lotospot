//
//  LandBarsTableViewController.swift
//  LotoSpot
//
//  Created by Bret Rideout on 5/24/16.
//  Copyright © 2016 RapidWare. All rights reserved.
//

import UIKit
import Firebase
import MBProgressHUD
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}



class LandBarsTableViewController: UITableViewController {

    var bars = [Bar]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.estimatedRowHeight = 214.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Log Out", style: .plain, target: self, action: #selector(self.logOut(_:)))
        self.tableView.backgroundColor = UIColor.init(red: CGFloat(241/255.0), green: CGFloat(241/255.0), blue: CGFloat(241/255.0), alpha: CGFloat(1.0))
        let activityIndicator = MBProgressHUD.showAdded(to: self.view, animated: true)
        activityIndicator?.labelText = "Loading Bars"
        activityIndicator?.isUserInteractionEnabled = false
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        DataService.ds.REF_LAND_BARS.observe(.value, with: { snapshot in
            
            self.bars = []
            if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                
                for snap in snapshots {
                    print("SNAP2: \(snap)")
                    
                    if let barDict = snap.value as? Dictionary<String, AnyObject> {
                        if barDict["visible"] as? Bool == true {
                            let key = snap.key
                            let bar = Bar(postKey: key, dictionary: barDict)
                            self.bars.append(bar)
                        }
                    }
                }
            }
            self.bars.sort(by: { $0.barCheckIns?.count > $1.barCheckIns?.count })
            self.tableView.reloadData()
            UIApplication.shared.endIgnoringInteractionEvents()
            activityIndicator?.hide(true)
            
        })
        
        //Add name
        if UserDefaults.standard.bool(forKey: KEY_ADMIN) {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.addTapped))
        }
        let userUID = UserDefaults.standard.object(forKey: KEY_UID) as! String
        
        DataService.ds.REF_USERS.child(userUID).observeSingleEvent(of: .value, with: { snapshot in
            if ((snapshot.value as? NSDictionary)?["name"] as? String == nil) {
                print((snapshot.value as? NSDictionary)?["name"])
                let alertController = UIAlertController(title: "Enter Name", message: nil, preferredStyle: .alert)
                
                let loginAction = UIAlertAction(title: "Enter", style: .default) { (_) in
                    let loginTextField = alertController.textFields![0] as UITextField
                    
                    var bar = String()
                    
                    bar = loginTextField.text!
                    
                    if let userId = UserDefaults.standard.value(forKey: KEY_UID) as? String {
                        let firebaseName = DataService.ds.REF_USERS.child("\(userId)/name")
                        firebaseName.setValue(bar)
                        UserDefaults.standard.setValue(loginTextField.text!, forKey: "userName")
                    }
                }
                
                loginAction.isEnabled = false
                
                //   let forgotPasswordAction = UIAlertAction(title: "Forgot Password", style: .Destructive) { (_) in }
                //   let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (_) in }
                
                alertController.addTextField { (textField) in
                    textField.placeholder = "Enter Name"
                    
                    NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: textField, queue: OperationQueue.main) { (notification) in
                        loginAction.isEnabled = textField.text != ""
                    }
                }
                alertController.addAction(loginAction)
                
                self.present(alertController, animated: true) {
                    // ...
                }
            } else {
                UserDefaults.standard.setValue((snapshot.value as? NSDictionary)!["name"], forKey: "userName")
                print((snapshot.value as? NSDictionary)!["name"])
            }
            
        })
        
        if UserDefaults.standard.value(forKey: "userName") == nil {
            
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        landBar = true
        waterBar = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.title = "Land Bars/Resturants"
    }
    
    @IBAction func logOut(_ sender: AnyObject) {
        
        try! FIRAuth.auth()!.signOut()
        UserDefaults.standard.setValue(nil, forKey: KEY_UID)
        UserDefaults.standard.setValue(nil, forKey: "userName")
        performSegue(withIdentifier: SEGUE_UNWIND_LOGIN, sender: self)
        
    }
    
    @IBAction func addTapped(_ sender: UIButton!) {
        performSegue(withIdentifier: SEGUE_ADD_BAR, sender: self)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return bars.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let bar = bars[indexPath.row]
        if let cell = tableView.dequeueReusableCell(withIdentifier: CELL_LAND_BAR_CELL) as? LandBarCell {
            cell.selectionStyle = .none
            if let barImgURL = bar.barImage {
                if let img = LandBarDetailTableViewController.imageCache.object(forKey: barImgURL) {
                    cell.configureCell(bar, img: img)
                    return cell
                } else {
                    cell.configureCell(bar)
                    return cell
                }
            } else {
                cell.configureCell(bar)
                return cell
            }
        } else {
            return LandBarCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            performSegue(withIdentifier: SEGUE_LAND_BAR_DETAIL, sender: self)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
    // MARK: - Navigation
    
    @IBAction func unwindToLandBar(segue: UIStoryboardSegue) {
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == SEGUE_LAND_BAR_DETAIL {
            if let destination = segue.destination as? LandBarDetailTableViewController {
                let indexPath = tableView.indexPathForSelectedRow!.row
                print(bars[indexPath])
                destination.bar = bars[indexPath]
                destination.bars.append(bars[indexPath])
                destination.barType = "land"
                self.navigationItem.title = nil
            }
        }
    }
}

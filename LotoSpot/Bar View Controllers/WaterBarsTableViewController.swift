//
//  WaterBarsTableViewController.swift
//  LotoSpot
//
//  Created by Bret Rideout on 5/24/16.
//  Copyright © 2016 RapidWare. All rights reserved.
//

import UIKit
import Firebase
import MBProgressHUD
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}



class WaterBarsTableViewController: UITableViewController {
    
    var bars = [Bar]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.estimatedRowHeight = 214.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.backgroundColor = UIColor.init(red: CGFloat(241/255.0), green: CGFloat(241/255.0), blue: CGFloat(241/255.0), alpha: CGFloat(1.0))
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        let activityIndicator = MBProgressHUD.showAdded(to: self.view, animated: true)
        
        activityIndicator?.labelText = "Loading Bars"
        activityIndicator?.isUserInteractionEnabled = false
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        DataService.ds.REF_WATER_BARS.observe(.value, with: { snapshot in
            print(snapshot.value)
            
            self.bars = []
            if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                
                for snap in snapshots {
                    print("SNAP: \(snap)")
                    
                    if let barDict = snap.value as? Dictionary<String, AnyObject> {
                        if barDict["visible"] as? Bool == true {
                            let key = snap.key
                            let bar = Bar(postKey: key, dictionary: barDict)
                            self.bars.append(bar)
                        }
                    }
                }
            }
            self.bars.sort(by: { $0.barCheckIns?.count > $1.barCheckIns?.count })
            self.tableView.reloadData()
            UIApplication.shared.endIgnoringInteractionEvents()
            activityIndicator?.hide(true)
            
        })
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        landBar = false
        waterBar = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.title = "Water Bars/Resturants"
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: SEGUE_WATER_BAR_DETAIL, sender: self)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return bars.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let bar = bars[indexPath.row]
        if let cell = tableView.dequeueReusableCell(withIdentifier: CELL_WATER_BAR_CELL) as? BarCell {
            cell.selectionStyle = .none
            if let barImgURL = bar.barImage {
                if let img = LandBarDetailTableViewController.imageCache.object(forKey: barImgURL) {
                    cell.configureCell(bar, img: img)
                    return cell
                } else {
                    cell.configureCell(bar)
                    return cell
                }
            } else {
                cell.configureCell(bar)
                return cell
            }
        } else {
            return BarCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
    // MARK: - Navigation
    
    @IBAction func unwindToWaterBar(segue: UIStoryboardSegue) {
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SEGUE_WATER_BAR_DETAIL {
            if let destination = segue.destination as? LandBarDetailTableViewController {
                let indexPath = tableView.indexPathForSelectedRow!.row
                print(bars[indexPath])
                destination.bar = bars[indexPath]
                destination.bars.append(bars[indexPath])
                destination.barType = "water"
                self.navigationItem.title = nil
            }
        }
    }
}

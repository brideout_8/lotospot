//
//  BarTypeViewController.swift
//  LotoSpot
//
//  Created by Bret Rideout on 5/22/16.
//  Copyright © 2016 RapidWare. All rights reserved.
//

import UIKit
import Firebase

class BarTypeViewController: UIViewController {
    
    let alert = UIAlertController(title: "Share Quote", message: "Enter an email address", preferredStyle: UIAlertControllerStyle.alert)
    var name = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userUID = UserDefaults.standard.object(forKey: KEY_UID) as! String
        
        DataService.ds.REF_USERS.child(userUID).observeSingleEvent(of: .value, with: { snapshot in
            print("Look\(snapshot.value)")
            
            if ((snapshot.value as? NSDictionary)?["admin"] as? Int == 1) {
                UserDefaults.standard.set(true, forKey: KEY_ADMIN)
                self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.addTapped))
            } else {
                UserDefaults.standard.set(false, forKey: KEY_ADMIN)
            }
            
            if (((snapshot.value as? NSDictionary)?["name"] as? String) == nil) {
                print((snapshot.value as? NSDictionary)?["name"])
                let alertController = UIAlertController(title: "Enter Name", message: nil, preferredStyle: .alert)
                
                let loginAction = UIAlertAction(title: "Enter", style: .default) { (_) in
                    let loginTextField = alertController.textFields![0] as UITextField
                    
                    var bar = String()
                    
                    bar = loginTextField.text!
                    
                    if let userId = UserDefaults.standard.value(forKey: KEY_UID) as? String {
                        let firebaseName = DataService.ds.REF_USERS.child("\(userId)/name")
                        firebaseName.setValue(bar)
                        UserDefaults.standard.setValue(loginTextField.text!, forKey: "userName")
                    }
                }
                
                loginAction.isEnabled = false
                
                //   let forgotPasswordAction = UIAlertAction(title: "Forgot Password", style: .Destructive) { (_) in }
                //   let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (_) in }
                
                alertController.addTextField { (textField) in
                    textField.placeholder = "Enter Name"
                    
                    NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: textField, queue: OperationQueue.main) { (notification) in
                        loginAction.isEnabled = textField.text != ""
                    }
                }
                alertController.addAction(loginAction)
                
                self.present(alertController, animated: true) {
                    // ...
                }
            } else {
                UserDefaults.standard.setValue((snapshot.value as? NSDictionary)!["name"], forKey: "userName")
                print((snapshot.value as? NSDictionary)!["name"])
            }
            
            })
        
        if UserDefaults.standard.value(forKey: "userName") == nil {
            
//            let alert = UIAlertController(title: "Enter Your Name", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
            
            
         /*   alertController.addTextFieldWithConfigurationHandler { (textField) in
                textField.placeholder = "Password"
                textField.secureTextEntry = true
            }*/
            
            
        
        /*
            alert.addTextFieldWithConfigurationHandler(configurationTextField)
//            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler:handleCancel))
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler:{ (UIAlertAction)in
                
                if tField.text == "" {
                    UIAlertAction.enabled = false
                } else {
                    self.name = tField.text!
                }
                
            }))
            self.presentViewController(alert, animated: true, completion: {
                print("completion block", terminator: "")
            })*/
            
        }

        // Do any additional setup after loading the view.
    }

    @IBAction func logOut(_ sender: AnyObject) {
        
        try! FIRAuth.auth()!.signOut()
        UserDefaults.standard.setValue(nil, forKey: KEY_UID)
        UserDefaults.standard.setValue(nil, forKey: "userName")
        performSegue(withIdentifier: SEGUE_UNWIND_LOGIN, sender: self)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onWater(_ sender: UIButton!) {
        DataService.ds.REF_WATER_BARS.observeSingleEvent(of: .value, with: { snapshot in
            
            if (snapshot.value != nil) {
                self.performSegue(withIdentifier: SEGUE_WATER_BAR, sender: self)
            } else {
                showErrorAlert("", msg: "No water bars at this time", View: self)
            }
        
        })
    }
    
    @IBAction func onLand(_ sender: UIButton!) {
        DataService.ds.REF_LAND_BARS.observeSingleEvent(of: .value, with: { snapshot in
            
            if (snapshot.value != nil) {
                self.performSegue(withIdentifier: SEGUE_LAND_BAR, sender: self)
            } else {
                showErrorAlert("", msg: "No land bars at this time", View: self)
            }
            
        })
    }
    
    @IBAction func addTapped(_ sender: UIButton!) {
        performSegue(withIdentifier: SEGUE_ADD_BAR, sender: self)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  DataService.swift
//  LotoSpot
//
//  Created by Bret Rideout on 5/21/16.
//  Copyright © 2016 RapidWare. All rights reserved.
//

import Foundation
import Firebase

let URL_BASE = FIRDatabase.database().reference()
let STORAGE_BASE = FIRStorage.storage().reference()
var ref:FIRDatabaseReference!

class DataService {
    static let ds = DataService()
    
    fileprivate var _REF_BASE = URL_BASE
    private var _REF_BAR_IMAGES = STORAGE_BASE.child("bar-pics")
    
    var REF_BASE: FIRDatabaseReference {
        return _REF_BASE
    }
    
    var REF_USERS: FIRDatabaseReference {
        let users = _REF_BASE.child("users")
        return users
    }
    
    var REF_USER_CURRENT: FIRDatabaseReference {
        let uid = UserDefaults.standard.value(forKey: KEY_UID) as! String
        let user = _REF_BASE.child("users").child(uid)
        return user
    }
    
    var REF_LAND_BARS: FIRDatabaseReference {
        let landBars = _REF_BASE.child(KEY_LAND_BARS)
        return landBars
    }
    
    var REF_WATER_BARS: FIRDatabaseReference {
        let waterBars = _REF_BASE.child(KEY_WATER_BARS)
        return waterBars
    }
    
    var REF_WATER_CHECK_INS: FIRDatabaseReference {
        let waterUid = UserDefaults.standard.value(forKey: KEY_WATER_BAR_UID) as! String
        let checkIns = _REF_BASE.child(KEY_WATER_BARS).child(waterUid).child("checkIns")
        return checkIns
    }
    
    var REF_LAND_CHECK_INS: FIRDatabaseReference {
        let landUid = UserDefaults.standard.value(forKey: KEY_LAND_BAR_UID) as! String
        let checkIns = _REF_BASE.child(KEY_LAND_BARS).child(landUid).child("checkIns")
        return checkIns
    }
    
    var REF_LAND_BARS_REVIEWS: FIRDatabaseReference {
        let landUid = UserDefaults.standard.value(forKey: KEY_LAND_BAR_UID) as! String
        let landReviews = _REF_BASE.child(KEY_LAND_BARS).child(landUid).child("reviews")
        return landReviews
    }
    
    var REF_WATER_BARS_REVIEWS: FIRDatabaseReference {
        let waterUid = UserDefaults.standard.value(forKey: KEY_WATER_BAR_UID) as! String
        let waterReviews = _REF_BASE.child(KEY_WATER_BARS).child(waterUid).child("reviews")
        return waterReviews
    }
    
    var REF_CAB_TAXIS: FIRDatabaseReference {
        let cabTaxis = _REF_BASE.child(KEY_CAB_TAXI)
        return cabTaxis
    }
    
    var REF_WATER_TAXIS: FIRDatabaseReference {
        let waterTaxis = _REF_BASE.child(KEY_WATER_TAXI)
        return waterTaxis
    }
    
    var REF_BAR_IMAGES: FIRStorageReference {
        return _REF_BAR_IMAGES
    }
    
    func createFirebaseUser(_ uid: String, user: Dictionary<String, String>) {
        REF_USERS.child(uid).setValue(user)
    }
}

//
//  HiddenBarTableViewController.swift
//  LotoSpot
//
//  Created by Bret Rideout on 3/30/17.
//  Copyright © 2017 RapidWare. All rights reserved.
//

import UIKit
import MBProgressHUD
import Firebase
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}

class HiddenBarTableViewController: UITableViewController {
    
    var bars = [Bar]()
    var activityIndicator = MBProgressHUD()
    var barType = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.estimatedRowHeight = 214.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.backgroundColor = UIColor.init(red: CGFloat(241/255.0), green: CGFloat(241/255.0), blue: CGFloat(241/255.0), alpha: CGFloat(1.0))
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        activityIndicator = MBProgressHUD.showAdded(to: self.view, animated: true)
        activityIndicator.labelText = "Loading Bars"
        activityIndicator.isUserInteractionEnabled = false
        UIApplication.shared.beginIgnoringInteractionEvents()
        if self.barType == "water" {
            DataService.ds.REF_WATER_BARS.observe(.value, with: { snapshot in
                self.bars = []
                if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                    
                    for snap in snapshots {
                        print("SNAP: \(snap)")
                        
                        if let barDict = snap.value as? Dictionary<String, AnyObject> {
                            if barDict["visible"] as? Bool == false {
                                let key = snap.key
                                let bar = Bar(postKey: key, dictionary: barDict)
                                self.bars.append(bar)
                            }
                        }
                    }
                }
                self.bars.sort(by: { $0.barCheckIns?.count > $1.barCheckIns?.count })
                self.tableView.reloadData()
                UIApplication.shared.endIgnoringInteractionEvents()
                self.activityIndicator.hide(true)
            })
        } else {
            DataService.ds.REF_LAND_BARS.observe(.value, with: { snapshot in
                
                if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                    
                    for snap in snapshots {
                        
                        if let barDict = snap.value as? Dictionary<String, AnyObject> {
                            if barDict["visible"] as? Bool == false {
                                let key = snap.key
                                let bar = Bar(postKey: key, dictionary: barDict)
                                self.bars.append(bar)
                            }
                        }
                    }
                }
                self.bars.sort(by: { $0.barCheckIns?.count > $1.barCheckIns?.count })
                self.tableView.reloadData()
                UIApplication.shared.endIgnoringInteractionEvents()
                self.activityIndicator.hide(true)
            })
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return bars.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: SEGUE_HIDDEN_BAR_DETAIL, sender: self)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let bar = bars[indexPath.row]
        if let cell = tableView.dequeueReusableCell(withIdentifier: CELL_HIDDEN_BAR_CELL) as? HiddenBarCell {
            cell.selectionStyle = .none
            if let barImgURL = bar.barImage {
                if let img = LandBarDetailTableViewController.imageCache.object(forKey: barImgURL) {
                    cell.configureCell(bar, img: img)
                    return cell
                } else {
                    cell.configureCell(bar)
                    return cell
                }
            } else {
                cell.configureCell(bar)
                return cell
            }
        } else {
            return HiddenBarCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
    @IBAction func unwindToHiddenBar(segue: UIStoryboardSegue) {
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SEGUE_HIDDEN_BAR_DETAIL {
            if let destination = segue.destination as? LandBarDetailTableViewController {
                let indexPath = tableView.indexPathForSelectedRow!.row
                print(bars[indexPath])
                destination.bar = bars[indexPath]
                destination.bars.append(bars[indexPath])
                destination.barType = self.barType
                destination.hiddenBar = true
            }
        }
    }
}

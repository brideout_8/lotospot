//
//  PasswordResetViewController.swift
//  LotoSpot
//
//  Created by Bret Rideout on 3/13/17.
//  Copyright © 2017 RapidWare. All rights reserved.
//

import UIKit
import Firebase

class PasswordResetViewController: UIViewController {

    @IBOutlet weak var emailAddress: MaterialTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func sendLink(_ sender: Any) {
        
        if self.emailAddress.text == "" {
            showErrorAlert("Error", msg: "Please enter an email address", View: self)
        } else {
            FIRAuth.auth()?.sendPasswordReset(withEmail: emailAddress.text!, completion: { (error) in
                if error != nil {
                    DispatchQueue.main.async {
                        showErrorAlert("Error", msg: (error?.localizedDescription)!, View: self)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.emailAddress.text = ""
                        self.successAlert()
                    }
                }
            })
        }
        
    }
    func successAlert() {
        let alert = UIAlertController(title: "Success", message: "Password reset email sent.", preferredStyle: .alert)
        //                    let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        let action = UIAlertAction(title: "Ok", style: .default, handler: { (nil) in
            self.performSegue(withIdentifier: SEGUE_UNWIND_LOGIN, sender: self)
        })
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
}

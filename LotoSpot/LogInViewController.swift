//
//  ViewController.swift
//  LotoSpot
//
//  Created by Bret Rideout on 5/21/16.
//  Copyright © 2016 RapidWare. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit
import FBSDKCoreKit
import MBProgressHUD

class LogInViewController: UIViewController {
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    let loginButton = FBSDKLoginButton()

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if UserDefaults.standard.value(forKey: KEY_UID) != nil {
            performSegue(withIdentifier: SEGUE_LOGGED_IN, sender: self)
        }
    }
    
    @IBAction func fbBtnPressed(_ sender: UIButton!) {
    
        let facebookLogin = FBSDKLoginManager()

        facebookLogin.logIn(withReadPermissions: ["email"], from:self, handler: { (result, error) -> Void in
        
//         facebookLogin.logInWithReadPermissions(facebookReadPermissions, handler: { (result:FBSDKLoginManagerLoginResult!, error:NSError!) -> Void in
        
//        facebookLogin.logIn(withReadPermissions: ["email"], from: self) { (facebookResult: FBSDKLoginManagerLoginResult!, error: Error!) in
        
//        facebookLogin.logIn(withReadPermissions: ["email"], from: self) { (facebookResult: FBSDKLoginManagerLoginResult!, facebookError: Error!) in
            
            if error != nil {
                print(error)
                showErrorAlert("Error", msg: "Failed to log into Facebook. Try again later. Error \(error)", View: self)
                //Handle Error
            } else {
                let credential = FIRFacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
                print(credential)
                FIRAuth.auth()?.signIn(with: credential) { (user, error) in
                    
                    if error != nil {
                        print("Login failed \(error)")
                    } else {
                        UserDefaults.standard.setValue(user!.uid, forKey: KEY_UID)
                        if let name = user!.displayName {
                            if let email = user!.email {
                                let userInfo = ["email": email, "name": name, "provider": user!.providerID]
                                DataService.ds.createFirebaseUser(user!.uid, user: userInfo)
                                UserDefaults.standard.setValue(user!.uid, forKey: KEY_UID)
                                UserDefaults.standard.setValue(name, forKey: "userName")
                                self.emailField.text = ""
                                self.passwordField.text = ""
                                self.performSegue(withIdentifier: SEGUE_LOGGED_IN, sender: self)
                            } else {
                                let userInfo = ["provider": user!.providerID]
                                DataService.ds.createFirebaseUser(user!.uid, user: userInfo)
                                UserDefaults.standard.setValue(user!.uid, forKey: KEY_UID)
                                self.emailField.text = ""
                                self.passwordField.text = ""
                                self.performSegue(withIdentifier: SEGUE_LOGGED_IN, sender: self)
                            }
                        }
                    }
                }
            }
        })
    }
    
    @IBAction func signUpLogIn(_ sender: UIButton!) {
        
        if let email = emailField.text, email != "", let pwd = passwordField.text, pwd != "" {
            let activityIndicator = MBProgressHUD.showAdded(to: self.view, animated: true)
            activityIndicator?.labelText = "Logging In"
            activityIndicator?.isUserInteractionEnabled = false
            UIApplication.shared.beginIgnoringInteractionEvents()
            FIRAuth.auth()?.signIn(withEmail: email, password: pwd, completion: { (authData, error) in
                
                if error != nil {
                    print(error!._code)
                    if error!._code == STATUS_ACCOUNT_NONEXIST {
                        
                        FIRAuth.auth()?.createUser(withEmail: email, password: pwd, completion: { (result, error) in
                            if error != nil {
                                //Display Error for not long enough password
                                UIApplication.shared.endIgnoringInteractionEvents()
                                activityIndicator?.hide(true)
                                showErrorAlert("Error", msg: "Password must be 6 characters long.", View: self)
                            } else {
                                UserDefaults.standard.setValue(result!.uid, forKey: KEY_UID)
                                FIRAuth.auth()?.signIn(withEmail: email, password: pwd, completion: { (user, err) in
                                    
                                    let userInfo = ["email": email, "provider": user!.providerID]
                                    DataService.ds.createFirebaseUser(user!.uid, user: userInfo)
                                    UserDefaults.standard.setValue(user!.uid, forKey: KEY_UID)
                                    UserDefaults.standard.set(false, forKey: KEY_ADMIN)
                                    UIApplication.shared.endIgnoringInteractionEvents()
                                    activityIndicator?.hide(true)
                                    self.emailField.text = ""
                                    self.passwordField.text = ""
                                    self.performSegue(withIdentifier: SEGUE_LOGGED_IN, sender: self)
                                })
                            }
                        })
                    } else {
                        
                        if let errorCode = FIRAuthErrorCode(rawValue: (error?._code)!) {
//                            if errorCode == .errorCodeEmailAlreadyInUse {
//                                showErrorAlert("Error", msg: "Email address already in use", View: self)
//                            } else if errorCode == .errorCodeInvalidEmail {
//                                showErrorAlert("Error", msg: "Invalid email address", View: self)
//                            } else if errorCode == .errorCodeWrongPassword {
//                                showErrorAlert("Error", msg: "Password is incorrect", View: self)
//                            } else if errorCode == .errorCodeWeakPassword {
//                                showErrorAlert("Error", msg: "Password needs to be a miniumum of 6 characters", View: self)
//                            } else {
//                                showErrorAlert("Error", msg: "Error code: \(error!._code)", View: self)
//                            }
                            switch (errorCode) {
                            case .errorCodeEmailAlreadyInUse:
                                UIApplication.shared.endIgnoringInteractionEvents()
                                activityIndicator?.hide(true)
                                showErrorAlert("Error", msg: "Email address already in use", View: self)
                                break
                            case .errorCodeInvalidEmail:
                                UIApplication.shared.endIgnoringInteractionEvents()
                                activityIndicator?.hide(true)
                                showErrorAlert("Error", msg: "Invalid email address", View: self)
                                break
                            case .errorCodeWrongPassword:
                                UIApplication.shared.endIgnoringInteractionEvents()
                                activityIndicator?.hide(true)
                                showErrorAlert("Error", msg: "Password is incorrect", View: self)
                                break
                            case .errorCodeWeakPassword:
                                UIApplication.shared.endIgnoringInteractionEvents()
                                activityIndicator?.hide(true)
                                showErrorAlert("Error", msg: "Password needs to be a miniumum of 6 characters", View: self)
                                break
                            default:
                                UIApplication.shared.endIgnoringInteractionEvents()
                                activityIndicator?.hide(true)
                                showErrorAlert("Error", msg: "Error code: \(error!._code)", View: self)
                            }
                        }
                    }
                } else {
                    
                    DataService.ds.REF_USERS.child(authData!.uid).observeSingleEvent(of: .value, with: { snapshot in
                        if ((snapshot.value as? NSDictionary)?["admin"] as? String != nil) {
                            UserDefaults.standard.set(true, forKey: KEY_ADMIN)
                        } else {
                            UserDefaults.standard.set(false, forKey: KEY_ADMIN)
                        }
                        UIApplication.shared.endIgnoringInteractionEvents()
                        activityIndicator?.hide(true)
                        UserDefaults.standard.setValue(authData!.uid, forKey: KEY_UID)
                        self.emailField.text = ""
                        self.passwordField.text = ""
                        self.performSegue(withIdentifier: SEGUE_LOGGED_IN, sender: self)
                    })
                }
            })
            
        } else {
            showErrorAlert("Error", msg: "Email and Password cannot be empty", View: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationTabBar = segue.destination as? UITabBarController {
            if UserDefaults.standard.bool(forKey: KEY_ADMIN) == false {
                destinationTabBar.viewControllers?.remove(at: 3)
            }
        }
    }
    
    @IBAction func logIn(_ segue:UIStoryboardSegue) {
        self.emailField.text = ""
        self.passwordField.text = ""
    }
}


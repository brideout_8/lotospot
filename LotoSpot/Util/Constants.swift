//
//  Constants.swift
//  LotoSpot
//
//  Created by Bret Rideout on 5/22/16.
//  Copyright © 2016 RapidWare. All rights reserved.
//

import Foundation
import UIKit

//Keys
let KEY_UID = "uid"
let KEY_LAND_BARS = "landBars"
let KEY_WATER_BARS = "waterBars"
let KEY_LAND_BAR_UID = "landUid"
let KEY_WATER_BAR_UID = "waterUid"
let KEY_ADMIN = "admin"
let KEY_CAB_TAXI = "cabTaxis"
let KEY_WATER_TAXI = "waterTaxis"

//Bools
var waterBar = false
var landBar = false

//Cells
let CELL_LAND_BAR_CELL = "landBarCell"
let CELL_WATER_BAR_CELL = "waterBarCell"
let CELL_LAND_BAR_DETAIL_CELL = "landBarDetailCell"
let CELL_WATER_BAR_DETAIL_CELL = "waterBarDetailCell"
let CELL_BAR_MENU_CELL = "barMenuCell"
let CELL_BAR_COUPONS_CELL = "barCouponCell"
let CELL_BAR_EVENT_CELL = "barEventCell"
let CELL_BAR_REVIEW_CELL = "barReviewCell"
let CELL_TAXI_DETAIL_CELL = "taxiCell"
let CELL_HIDDEN_BAR_CELL = "hiddenBarCell"

//Segues
let SEGUE_LOGGED_IN = "loggedIn"
let SEGUE_WATER_BAR = "waterBar"
let SEGUE_LAND_BAR = "landBar"
let SEGUE_LAND_BAR_DETAIL = "landBarDetail"
let SEGUE_WATER_BAR_DETAIL = "waterBarDetail"
let SEGUE_BAR_MENU = "barMenu"
let SEGUE_BAR_COUPONS = "barCoupons"
let SEGUE_BAR_EVENTS = "barEvents"
let SEGUE_BAR_REVIEWS = "barReviews"
let SEGUE_BAR_PARKING = "parkingSegue"
let SEGUE_BAR_ABOUT_US = "aboutUs"
let SEGUE_BAR_MAP = "barMap"
let SEGUE_ADD_BAR = "addBarSegue"
let SEGUE_BAR_MENU_ADD = "barMenuAdd"
let SEGUE_BAR_COUPONS_ADD = "barCuoponsAdd"
let SEGUE_BAR_EVENTS_ADD = "barEventsAdd"
let SEGUE_BAR_COUPONS_ADD2 = "barCouponsAdd2"
let SEGUE_BAR_MENU_ADD2 = "barMenuAdd2"
let SEGUE_BAR_EVENTS_ADD2 = "barEventsAdd2"
let SEGUE_UNWIND_LOGIN = "unwindLogIn"
let SEGUE_EDIT_BAR = "editBar"
let SEGUE_UNWIND_TO_LAND_BAR = "unwindToLandBar"
let SEGUE_UNWIND_TO_WATER_BAR = "unwindToWaterBar"
let SEGUE_TAXI_DETAIL = "taxiDetail"
let SEGUE_ADD_TAXI = "addTaxi"
let SEGUE_HIDDEN_BAR_DETAIL = "hiddenBarDetail"
let SEGUE_HIDDEN_BARS = "hiddenBarsSegue"
let SEGUE_UNWIND_TO_HIDDEN_BAR = "unwindToHiddenBar"

//Status Codes
let STATUS_ACCOUNT_NONEXIST = 17011
let STATUS_PASSWORD_INCORRECT = 17009
let STATUS_NOT_AN_EMAIL = 17999

//Functions
func showErrorAlert(_ title: String, msg: String, View: UIViewController) {
    let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
    let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
    alert.addAction(action)
    View.present(alert, animated: true, completion: nil)
}

//Material 
let SHADOW_COLOR: CGFloat = 157.0 / 255.0

//Extensions

extension Date {
    
    func offsetFrom(_ date:Date) -> String {
        
        let dayHourMinuteSecond: NSCalendar.Unit = [.day, .hour, .minute, .second]
        let difference = (Calendar.current as NSCalendar).components(dayHourMinuteSecond, from: date, to: self, options: [])
        
        let seconds = "\(difference.second)s"
        let minutes = "\(difference.minute)m" + " " + seconds
        let hours = "\(difference.hour)h" + " " + minutes
        let days = "\(difference.day)d" + " " + hours
        
        if difference.day!    > 0 { return days }
        if difference.hour!   > 0 { return hours }
        if difference.minute! > 0 { return minutes }
        if difference.second! > 0 { return seconds }
        return ""
    }
}

extension UIImageView{
    func roundedImage(){
        let maskPAth1 = UIBezierPath(roundedRect: self.bounds,
                                     byRoundingCorners: [.topLeft , .topRight],
                                     cornerRadii:CGSize(width: 5.0, height: 5.0))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = self.bounds
        maskLayer1.path = maskPAth1.cgPath
        self.layer.mask = maskLayer1
        
    }
}


//
//  AddBarTableViewController.swift
//  LotoSpot
//
//  Created by Debra Rideout on 6/5/16.
//  Copyright © 2016 RapidWare. All rights reserved.
//

import UIKit
import Firebase

class AddBarTableViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var barName: UITextField!
    @IBOutlet weak var barAddress: UITextField!
    @IBOutlet weak var barLat: UITextField!
    @IBOutlet weak var barLon: UITextField!
    @IBOutlet weak var barAboutUs: UITextView!
    @IBOutlet weak var mondayHours: UITextField!
    @IBOutlet weak var tuesdayHours: UITextField!
    @IBOutlet weak var wenesdayHours: UITextField!
    @IBOutlet weak var thursdayHours: UITextField!
    @IBOutlet weak var fridayHours: UITextField!
    @IBOutlet weak var saturdayHours: UITextField!
    @IBOutlet weak var sundayHours: UITextField!
    @IBOutlet weak var uncoveredParking: UITextField!
    @IBOutlet weak var coveredParking: UITextField!
    @IBOutlet weak var slipLengths: UITextField!
    @IBOutlet weak var waterBarSwitch: UISwitch!
    @IBOutlet weak var barMenuSite: UITextField!
    @IBOutlet weak var barPhoneNumber: UITextField!
    @IBOutlet weak var barWebsite: UITextField!
    @IBOutlet weak var visibleSwith: UISwitch!
    @IBOutlet weak var barImage: UIImageView!
    
    var imagePicker: UIImagePickerController!
    var imageSelected = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imagePicker = UIImagePickerController()
        self.imagePicker.allowsEditing = true
        self.imagePicker.delegate = self

    }
    
    @IBAction func addImage(_ sender: AnyObject) {
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.barImage.image = image
            self.imageSelected = true
        } else {
            print("BRET: A valid image was not selected")
        }
        self.imagePicker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveTabbed(_ sender: AnyObject) {
        guard let name = barName.text, name != "", let address = barAddress.text, address != "", let lat = barLat.text, lat != "", let lon = barLon.text, lon != "", let aboutUs = barAboutUs.text, aboutUs != "", let monday = mondayHours.text, monday != "", let tuesday = tuesdayHours.text, tuesday != "", let wenesday = wenesdayHours.text, wenesday != "", let thursday = thursdayHours.text, thursday != "", let friday = fridayHours.text, friday != "", let saturday = saturdayHours.text, saturday != "", let sunday = sundayHours.text, sunday != "", let menuSite = barMenuSite.text, menuSite != "", let phoneNumber = barPhoneNumber.text, phoneNumber != "", let website = barWebsite.text, website != ""  else {
            showErrorAlert("Error", msg: "No field can be blank.", View: self)
            return
        }
        
        guard let img = barImage.image, imageSelected == true else {
            print("BRET: No image selected")
            showErrorAlert("Error", msg: "An image must be selected", View: self)
            return
        }
        
        if let imgData = UIImageJPEGRepresentation(img, 0.2) {
            let imgUID = NSUUID().uuidString
            let metaData = FIRStorageMetadata()
            metaData.contentType = "image/jpeg"
            DataService.ds.REF_BAR_IMAGES.child(imgUID).put(imgData, metadata: metaData) { (metadata, error) in
                if error != nil {
                    showErrorAlert("Error", msg: "Error: \(error.debugDescription)", View: self)
                    return
                } else {
                    let imageURL = metadata?.downloadURL()?.absoluteString
                    if let url = imageURL {
                        self.postToFirebase(imgURL: url)
                    } else {
                        showErrorAlert("ERROR", msg: "Bad url", View: self)
                    }
                }
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 22
    }
    
    //MARK: - Helper Functions
    
    func postToFirebase(imgURL: String) {
        var bar: [String: Any] = [:]
        var bar2: [String: Any] = [:]
        var visible: Bool = true
        if visibleSwith.isOn {
            visible = true
        } else {
            visible = false
        }
        bar = ["hours":["monday": mondayHours.text!, "tuesday": tuesdayHours.text!, "wednesday": wenesdayHours.text!, "thursday": thursdayHours.text!, "friday":fridayHours.text!, "saturday":saturdayHours.text!, "sunday":sundayHours.text!], "coordinates":["lat":barLat.text!, "lon":barLon.text!], "parking":["covered":coveredParking.text!, "uncovered":uncoveredParking.text!, "length":slipLengths.text!], "name":barName.text!, "visible":visible, "desc":barAboutUs.text!, "address":barAddress.text!, "menuSite":barMenuSite.text!, "phoneNumber":barPhoneNumber.text!, "website":barWebsite.text!, "imageURL": imgURL]
        
        bar2 = ["hours":["monday": mondayHours.text!, "tuesday": tuesdayHours.text!, "wednesday": wenesdayHours.text!, "thursday": thursdayHours.text!, "friday":fridayHours.text!, "saturday":saturdayHours.text!, "sunday":sundayHours.text!], "coordinates":["lat":barLat.text!, "lon":barLon.text!], "name":barName.text!, "visible":visible, "desc":barAboutUs.text!, "address":barAddress.text!, "menuSite":barMenuSite.text!, "phoneNumber":barPhoneNumber.text!, "website":barWebsite.text!, "imageURL": imgURL]
        
        
        if waterBarSwitch.isOn == false {
            let firebaseReview = DataService.ds.REF_LAND_BARS.childByAutoId()
            firebaseReview.setValue(bar2, withCompletionBlock: { (error, result) in
                if error != nil {
                    showErrorAlert("Error", msg: "Error: \(error.debugDescription)", View: self)
                } else {
                    self.barSaved()
                }
            })
        } else {
        guard let convered = uncoveredParking.text, convered != "", let uncovered = uncoveredParking.text, uncovered != "", let lengths = slipLengths.text, lengths != "" else {
            showErrorAlert("Error", msg: "No field can be blank", View: self)
            return
        }
            let firebaseReview = DataService.ds.REF_WATER_BARS.childByAutoId()
            firebaseReview.setValue(bar, withCompletionBlock: { (error, result) in
                if error != nil {
                    showErrorAlert("Error", msg: "Error: \(error.debugDescription)", View: self)
                } else {
                    self.barSaved()
                }
            })
        }
    }
    
    func barSaved() {
        showErrorAlert("Success", msg: "Bar has been created", View: self)
        self.barName.text = ""
        self.barAddress.text = ""
        self.barLat.text = ""
        self.barLon.text = ""
        self.barAboutUs.text = ""
        self.mondayHours.text = ""
        self.tuesdayHours.text = ""
        self.wenesdayHours.text = ""
        self.thursdayHours.text = ""
        self.fridayHours.text = ""
        self.saturdayHours.text = ""
        self.sundayHours.text = ""
        self.uncoveredParking.text = ""
        self.coveredParking.text = ""
        self.slipLengths.text = ""
        self.barMenuSite.text = ""
        self.barPhoneNumber.text = ""
        self.barWebsite.text = ""
        self.imageSelected = false
        self.barImage.image = UIImage(named: "lotoSpot")
    }

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

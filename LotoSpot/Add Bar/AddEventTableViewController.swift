//
//  AddEventTableViewController.swift
//  LotoSpot
//
//  Created by Debra Rideout on 6/6/16.
//  Copyright © 2016 RapidWare. All rights reserved.
//

import UIKit

class AddEventTableViewController: UITableViewController {

    @IBOutlet weak var eventName: UITextField!
    @IBOutlet weak var eventDate: UITextField!
    @IBOutlet weak var eventTime: UITextField!
    
    var barKey = String()

    @IBAction func saveTabbed(_ sender: AnyObject) {
        
        if eventName.text! == "" || eventTime.text! == "" || eventDate.text! == "" {
            showErrorAlert("Error", msg: "No feilds can be left blank", View: self)
        } else if eventDate.text! == "" {
            showErrorAlert("Error", msg: "Date does not follow formate 02-15-2016", View: self)
        } else {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM-dd-yyyy"
            if dateFormatter.date(from: eventDate.text!) != nil {
                var bar: [String: AnyObject]
                
                bar = ["date":eventDate.text! as AnyObject, "name":eventName.text! as AnyObject, "time":eventTime.text! as AnyObject]
                
                if landBar {
                    let firebaseEvent = DataService.ds.REF_LAND_BARS.child("\(barKey)/events").childByAutoId()
                    firebaseEvent.setValue(bar)
                    showErrorAlert("Success", msg: "The event has been saved", View: self)
                    eventName.text = ""
                    eventTime.text = ""
                    eventDate.text = ""
                } else {
                    let firebaseEvent = DataService.ds.REF_WATER_BARS.child("\(barKey)/events").childByAutoId()
                    firebaseEvent.setValue(bar)
                    showErrorAlert("Success", msg: "The event has been saved", View: self)
                    eventName.text = ""
                    eventTime.text = ""
                    eventDate.text = ""
                }
            } else {
                showErrorAlert("Error", msg: "Date must be in format 02-30-2017", View: self)
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 4
    }
}
